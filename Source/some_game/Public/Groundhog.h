// cs24_team2 (c) BY

#pragma once

#include "GroundUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Groundhog.generated.h"

UCLASS()
class SOME_GAME_API AGroundhog : public AGroundUnit
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGroundhog();

	UFUNCTION(BlueprintCallable, Category = "Groundhog")
	bool CanSetCheckpoint() const;

	UFUNCTION(BlueprintCallable, Category = "Groundhog")
	void SetCheckpoint();

	UFUNCTION(BlueprintCallable, Category = "Groundhog")
	bool CanWarp() const;

	UFUNCTION(BlueprintCallable, Category = "Groundhog")
	void Warp();

	void ResetWarp();

	virtual bool CanAttack(ASomething* target) const override;

	virtual int32 GET_MAX_MOBILITY() const override;

	virtual int32 GET_MAX_ATTACK_COUNT() const override;

	virtual int32 GET_ATTACK_DAMAGE() const override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual int32 GET_VISIBILITY_DISTANCE() const override;

	virtual int32 GET_ATTACK_DISTANCE() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 ATTACK_DAMAGE = 20;
	UPROPERTY()
	int32 MAX_ATTACK_COUNT = 2;
	UPROPERTY()
	int32 MAX_HEALTH = 30;
	UPROPERTY()
	int32 MAX_MOBILITY = 1;
	UPROPERTY()
	int32 ATTACK_DISTANCE = 2;
	UPROPERTY()
	int32 VISIBLE_DISTANCE = 2;
	UPROPERTY()
	bool warp_ = false;
	UPROPERTY()
	ACell* checkpoint_cell_ = nullptr;
	UPROPERTY()
	int32 checkpoint_health_ = 0;
	UPROPERTY()
	int32 checkpoint_mobility_ = 0;
	UPROPERTY()
	int32 checkpoint_attack_count_ = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
