// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Something.generated.h"

class ACell;
class AGameplayGameMode;

UENUM()
enum class HighlightType : uint8 {
	NONE,
	ENEMY,
	FRIEND,
	SELECTED
};

UCLASS(Abstract)
class SOME_GAME_API ASomething : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASomething();

	void Init(ACell* cell, uint32 creation_turn, uint8 team, bool animate = true);

	void NextTurn();

	ACell* GetCell(uint32 turn) const;

	bool SetCell(ACell* cell, uint32 turn);

	int32 GetHealth(uint32 turn) const;

	UFUNCTION(BlueprintCallable, Category = "Something")
	int32 GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Something")
	uint8 GetTeam() const;

	void ModifyVisibleCellViewerCounts(uint32 turn, int32 value);

	void SetSomethingHidden(bool hidden);

	bool IsSomethingHidden() const;

	void Highlight(HighlightType type);

	const TArray<UStaticMeshComponent*>& GetMeshes() const;

	uint32& GetLastAnimationId();

	virtual void NotifyActorOnClicked(FKey ButtonPressed = EKeys::LeftMouseButton) override;

	virtual void Destroy(bool animate = true) PURE_VIRTUAL(ASomething::Destroy, );

	virtual bool IsVisible(ACell* cell, uint32 turn) const;

	virtual TArray<ACell*> GetVisibleCells(uint32 turn) const;

	virtual int32 Damage(int32 damage);

	virtual int32 Heal(int32 heal_amount);

	UFUNCTION(BlueprintCallable, Category = "Something")
	virtual int32 GET_MAX_HEALTH() const PURE_VIRTUAL(ASomething::GET_MAX_HEALTH, return 0;);

	UFUNCTION(BlueprintCallable, Category = "Something")
	virtual int32 GET_VISIBILITY_DISTANCE() const PURE_VIRTUAL(ASomething::GET_VISIBILITY_DISTANCE, return 0;);

	UFUNCTION(BlueprintCallable, Category = "Something")
	virtual FString GET_NAME() const PURE_VIRTUAL(ASomething::GET_NAME, return "";);

	UFUNCTION(BlueprintCallable, Category = "Something")
	void SET_STATS(FString stats);

	UFUNCTION(BlueprintCallable, Category = "Something")
	FString GET_STATS() const;

	UFUNCTION(BlueprintCallable, Category = "Something")
	void SET_DESCRIPTION(FString desc);

	UFUNCTION(BlueprintCallable, Category = "Something")
	FString GET_DESCRIPTION() const; 
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SetMesh(const ConstructorHelpers::FObjectFinder<UStaticMesh>& mesh);

	void SetMaterial(const ConstructorHelpers::FObjectFinder<UMaterial>& material);

	int32 DamagePrimitive(int32 damage);

	int32 HealPrimitive(int32 heal_amount);

	UPROPERTY()
	TArray<ACell*> underlying_cell_;
	UPROPERTY()
	TArray<int32> health_;
	UPROPERTY()
	uint8 team_ = 0;
	UPROPERTY()
	uint32 creation_turn_ = 0;
	UPROPERTY()
	uint32 current_turn_ = 0;
	UPROPERTY()
	uint32 destruction_turn_ = UINT32_MAX;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;

	UPROPERTY()
	TArray<UStaticMeshComponent*> meshes_;
	UPROPERTY()
	TArray<USceneComponent*> scene_components_;
	UPROPERTY()
	bool hidden_ = false;

	UPROPERTY()
	UMaterialInstanceDynamic* dynamic_highlight_material_ = nullptr;
	UPROPERTY()
	UMaterialInstanceDynamic* dynamic_team_material_ = nullptr;

	UPROPERTY()
	FString stats_;
	UPROPERTY()
	FString desc_;


	UPROPERTY()
	uint32 last_animation_id_ = UINT32_MAX;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
