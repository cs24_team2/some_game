// cs24_team2 (c) BY

#pragma once

#include "AerialUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Outcast.generated.h"

UCLASS()
class SOME_GAME_API AOutcast final : public AAerialUnit
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AOutcast();

	virtual void MoveTo(ACell* cell) override;

	virtual bool CanAttack(ASomething* target) const override;

	virtual int32 Attack(ASomething* target) override;

	void SetVisible();

	virtual int32 Damage(int32 damage) override;

	virtual int32 Heal(int32 heal_amount) override;

	virtual bool IsVisible(ACell* cell, uint32 turn) const override;

	virtual TArray<ACell*> GetVisibleCells(uint32 turn) const override;

	virtual int32 GET_MAX_MOBILITY() const override;

	virtual int32 GET_MAX_ATTACK_COUNT() const override;

	virtual int32 GET_ATTACK_DAMAGE() const override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 ATTACK_DAMAGE = 0;
	UPROPERTY()
	int32 MAX_ATTACK_COUNT = 0;
	UPROPERTY()
	int32 MAX_HEALTH = 100;
	UPROPERTY()
	int32 MAX_MOBILITY = 1;
	UPROPERTY()
	int32 ATTACK_DISTANCE = 0;
	UPROPERTY()
	int32 VISIBLE_DISTANCE = 1;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
