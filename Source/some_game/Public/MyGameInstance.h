// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

UCLASS()
class SOME_GAME_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "MyGameInstance")
	int32 team_count_ = 2;
	UPROPERTY(BlueprintReadWrite, Category = "MyGameInstance")
	int32 game_field_height_ = 10;
	UPROPERTY(BlueprintReadWrite, Category = "MyGameInstance")
	int32 game_field_width_ = 10;
};
