// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cell.generated.h"

class ABuilding;
class AGroundUnit;
class AAerialUnit;
class AGameplayGameMode;

UENUM()
enum class CellType : uint8 {
	WATER,
	GLACIER,
	RAISED_GLACIER,
	SNOWY_PLAINS,
	SNOWY_MOUNTAINS,
	PLAINS,
	MOUNTAINS,
	WASTELAND,
	DESERT,
	UNDEFINED
};

UCLASS()
class SOME_GAME_API ACell final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACell();

	void PropogateNextTurn();

	uint32& ViewerCounts(uint8 team, uint32 turn);

	void ModifyViewerCounts(uint8 team, uint32 turn, int32 value);

	void SetMaterialDesaturated(const bool desaturated);

	void SetCellHidden(bool hidden);

	void Highlight(bool highlight);

	virtual void NotifyActorOnClicked(FKey ButtonPressed = EKeys::LeftMouseButton) override;

	UPROPERTY()
	int32 x_ = 0;
	UPROPERTY()
	int32 y_ = 0;
	UPROPERTY()
	CellType type_ = CellType::UNDEFINED;

	UPROPERTY()
	TArray<ABuilding*> occupying_building_{ nullptr };
	UPROPERTY()
	TArray<AGroundUnit*> occupying_ground_unit_{ nullptr };
	UPROPERTY()
	TArray<AAerialUnit*> occupying_aerial_unit_{ nullptr };

	UPROPERTY()
	bool hidden_ = false;
	UPROPERTY()
	TArray<bool> discovered_;
	UPROPERTY()
	TArray<uint32> viewer_counts_;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;

	UPROPERTY()
	TArray<UStaticMeshComponent*> meshes_;
	UPROPERTY()
	TArray<UMaterialInstanceDynamic*> materials_;

	UPROPERTY()
	UMaterialInstance* highlight_overlay_material_ = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
