// cs24_team2 (c) BY

#pragma once

#include "AerialUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TransporterPod.generated.h"

UCLASS()
class SOME_GAME_API ATransporterPod final : public AAerialUnit
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATransporterPod();

	UFUNCTION(BlueprintCallable, Category = "TransporterPod")
	bool CanTransform() const;

	UFUNCTION(BlueprintCallable, Category = "TransporterPod")
	void Transform();

	virtual bool CanAttack(ASomething* target) const override;

	virtual int32 GET_MAX_MOBILITY() const override;

	virtual int32 GET_MAX_ATTACK_COUNT() const override;

	virtual int32 GET_ATTACK_DAMAGE() const override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual int32 GET_VISIBILITY_DISTANCE() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 ATTACK_DAMAGE = 0;
	UPROPERTY()
	int32 MAX_ATTACK_COUNT = 0;
	UPROPERTY()
	int32 MAX_HEALTH = 10;
	UPROPERTY()
	int32 MAX_MOBILITY = 1;
	UPROPERTY()
	int32 ATTACK_DISTANCE = 0;
	UPROPERTY()
	int32 VISIBLE_DISTANCE = 1;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
