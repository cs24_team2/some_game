// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "Resources.generated.h"

USTRUCT(BlueprintType)
struct SOME_GAME_API FResources
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "Resources")
	int32 h_ = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Resources")
	int32 c_ = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Resources")
	int32 ae_ = 0;

	FResources& operator+=(const FResources& other);
	FResources& operator-=(const FResources& other);
	bool operator<=(const FResources& other) const;
	bool operator>=(const FResources& other) const;
};
