// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CameraPawn.generated.h"

class UCameraComponent;
class AGameplayGameMode;

UCLASS()
class SOME_GAME_API ACameraPawn final : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACameraPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void UpdateCapValues(double half_distance_between_cells, int32 height, int32 width);

private:
	void MoveFB(float amount);

	void MoveLR(float amount);

	void Zoom(float amount);

	void OnSpaceBar();

	void OnEscape();

	UPROPERTY()
	UCameraComponent* camera_;

	UPROPERTY()
	FVector move_vector_;
	UPROPERTY()
	FVector zoom_vector_;

	UPROPERTY()
	double move_speed_ = 350.;
	UPROPERTY()
	double zoom_speed_ = 700.;

	UPROPERTY()
	double lower_bound_ = 250.;
	UPROPERTY()
	double upper_bound_ = 1400.;

	UPROPERTY()
	double y_cap_ = 0.;
	UPROPERTY()
	double x_cap_ = 0.;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;
};
