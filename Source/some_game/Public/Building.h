// cs24_team2 (c) BY

#pragma once

#include "Something.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Building.generated.h"

UCLASS(Abstract)
class SOME_GAME_API ABuilding : public ASomething
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuilding();

	virtual bool IsVisible(ACell* cell, uint32 turn) const override;

	virtual TArray<ACell*> GetVisibleCells(uint32 turn) const override;

	virtual void Destroy(bool animate = true) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
