// cs24_team2 (c) BY

#pragma once

#include "Building.h"

#include "Resources.h"
	
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Station.generated.h"

UCLASS()
class SOME_GAME_API AStation : public ABuilding
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStation();

	FResources GetResources() const;

	virtual int32 Damage(int32 damage) override;

	virtual int32 Heal(int32 heal_amount) override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 MAX_HEALTH = 60;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
