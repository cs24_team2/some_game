// cs24_team2 (c) BY

#pragma once

#include "Building.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Transporter.generated.h"

UCLASS()
class SOME_GAME_API ATransporter final : public ABuilding
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATransporter();

	virtual int32 Damage(int32 damage) override;

	virtual int32 Heal(int32 heal_amount) override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 MAX_HEALTH = 40;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
