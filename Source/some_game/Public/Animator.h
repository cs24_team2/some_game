// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Animator.generated.h"

class ACell;
class AGameplayGameMode;
class ASomething;
class UNiagaraSystem;

UENUM()
enum class EAnimationType : uint8
{
	MOVE,
	DESTROY,
	SHOOT,
	CREATE,
	TELEPORT,
	HEAL,
	DISAPPEAR
};

USTRUCT()
struct FAnimation
{
	GENERATED_BODY()

	UPROPERTY()
	EAnimationType type_;

	UPROPERTY()
	FVector from_;
	UPROPERTY()
	FVector to_;

	UPROPERTY()
	ASomething* a_;
	UPROPERTY()
	ASomething* b_;

	UPROPERTY()
	float start_time_;
	UPROPERTY()
	float end_time_;

	UPROPERTY()
	int32 value_ = 0;

	UPROPERTY()
	bool played_effect_ = false;
	UPROPERTY()
	bool played_effect_2_ = false;

	UPROPERTY()
	bool done_ = false;
};

UCLASS()
class SOME_GAME_API AAnimator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAnimator();

	uint32 AnimateMove(ASomething* something, const FVector& from, const FVector& to);

	uint32 AnimateTeleport(ASomething* something, const FVector& from, const FVector& to);

	uint32 AnimateCreate(ASomething* something, const FVector& to);

	uint32 AnimateDestroy(ASomething* something, float delay = 0.f);

	uint32 AnimateShoot(ASomething* attacker, ASomething* target, int32 damage);

	uint32 AnimateHeal(ASomething* healer, ASomething* target, int32 heal_amount);

	uint32 AnimateDisappear(ASomething* something);

	uint32 AddRawAnimation(EAnimationType type, const FVector& from, const FVector& to, ASomething* a, ASomething* b, float duration, int32 value = 0);

	void StopAnimation(uint32 id, bool end_position = true);

	bool AnimationInProgress() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TMap<uint32, FAnimation> animations_;

	UPROPERTY()
	uint32 next_id_ = 0;

	UPROPERTY()
	float time_ = 0.f;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;

	UPROPERTY()
	UNiagaraSystem* explosion_niagara_;
	UPROPERTY()
	UNiagaraSystem* creation_niagara_;
	UPROPERTY()
	UNiagaraSystem* teleport_disappear_niagara_;
	UPROPERTY()
	UNiagaraSystem* teleport_appear_niagara_;
	UPROPERTY()
	UNiagaraSystem* shoot_laser_niagara_;
	UPROPERTY()
	UNiagaraSystem* heal_laser_niagara_;

public:	
	// Called every frame
	virtual void Tick(float delta_time) override;

};
