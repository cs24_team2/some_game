// cs24_team2 (c) BY

#pragma once

#include "Resources.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameplayGameMode.generated.h"

class AReplayer;
class AGameField;
class ACell;
class ASomething;
class ACameraPawn;
class USomethingFactoryWidget;
class AAnimator;

USTRUCT()
struct FASomethingTArrayStruct
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<ASomething*> _;
};

USTRUCT()
struct F2DASomethingTArrayStruct
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FASomethingTArrayStruct> _;
};

UCLASS()
class SOME_GAME_API AGameplayGameMode final : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGameplayGameMode();

	AGameField* GetGameField() const;

	AAnimator* GetAnimator() const;

	AReplayer* GetReplayer() const;

	uint8 GetTeamCount() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	int32 GetDisplayTurn() const;

	uint32 GetCurrentTurn() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	uint8 GetCurrentTeam() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	FResources& GetTeamResources(const uint8 team);

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	ASomething* GetSelectedSomething() const;

	const TArray<ASomething*>& GetSomethingsOfClassAndTeam(const TSubclassOf<ASomething> subclass, const uint8 team);

	UStaticMesh* GetDefaultSomethingMesh() const;

	UMaterial* GetSomethingHighlightMaterial() const;

	FVector4 GetTeamColor(const uint8 team) const;

	int32 GetGameFieldHeight() const;

	int32 GetGameFieldWidth() const;

	double GetGameFieldHalfDistanceBetweenCells() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	ACameraPawn* GetCameraPawn() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	bool IsInbetweenTurns() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	void SetInbetweenTurns(const bool inbetween_turns);

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	bool DisplayTutorial() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	void SetDisplayTutorial(const bool display_tutorial);

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	bool DisplayGameOver() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	bool DisplayVictory() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	bool DisplayUI() const;

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	ASomething* SpawnFromWidget(USomethingFactoryWidget* widget);

	void ClickedOnCell(ACell* cell);

	void ClickedOnSomething(ASomething* something);

	void SetSelectedSomething(ASomething* something);

	UFUNCTION(BlueprintCallable, Category = "GameplayGameMode")
	void RequestNextTurn();

	void GlobalNextTurn();

	void GameOverForTeam(const uint8 team);

	bool IsTeamGameOver(const uint8 team) const;

	void AddSomething(ASomething* something, TSubclassOf<ASomething> subclass, uint8 team);

	void UpdateCellMeshes(ACell* cell, const uint32 turn) const;

	void UpdateAllMeshes(const uint32 turn) const;

	void PreReplay() const;

	void Replay();

	void PostReplay();

protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY()
	int32 game_field_height_ = 12;
	UPROPERTY()
	int32 game_field_width_ = 16;
	UPROPERTY()
	double game_field_half_distance_between_cells_ = 50.;
	UPROPERTY()
	double sqrt3 = UE_SQRT_3;
	UPROPERTY()
	AGameField* game_field_ = nullptr;

	UPROPERTY()
	uint8 team_count_ = 2;
	UPROPERTY()
	uint32 current_turn_ = 0;
	UPROPERTY()
	uint8 current_team_ = 0;
	UPROPERTY()
	TArray<bool> team_game_over_;
	UPROPERTY()
	TArray<bool> team_viewed_game_over_;
	UPROPERTY()
	uint8 remaining_team_count_ = team_count_;

	TArray<FResources> resources_;

	UPROPERTY()
	TArray<ASomething*> somethings_;

	UPROPERTY()
	TMap<TSubclassOf<ASomething>, F2DASomethingTArrayStruct> class_team_somethings_;

	UPROPERTY()
	ASomething* selected_something_ = nullptr;

	UPROPERTY()
	UStaticMesh* default_something_mesh_ = nullptr;

	UPROPERTY()
	UMaterial* something_highlight_material_ = nullptr;

	UPROPERTY()
	TArray<FVector> camera_locations_;
	UPROPERTY()
	ACameraPawn* camera_ = nullptr;

	UPROPERTY()
	TArray<ACell*> highlighted_cells_;
	UPROPERTY()
	TArray<ASomething*> highlighted_somethings_;

	UPROPERTY()
	bool inbetween_turns_ = false;
	UPROPERTY()
	bool display_tutorial_ = false;

	UPROPERTY()
	AAnimator* animator_ = nullptr;

	UPROPERTY()
	AReplayer* replayer_ = nullptr;
	UPROPERTY()
	TArray<uint32> replay_stack_size_at_end_of_turn_;
	UPROPERTY()
	bool replay_in_progress_ = false;
};
