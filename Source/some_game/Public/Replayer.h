// cs24_team2 (c) BY

#pragma once

#include "Animator.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Replayer.generated.h"

USTRUCT()
struct FSomethingChange
{
	GENERATED_BODY()

	UPROPERTY()
	ASomething* something_;

	UPROPERTY()
	bool hidden_;

	UPROPERTY()
	uint8 team_;
};

USTRUCT()
struct FCellChange
{
	GENERATED_BODY()

	UPROPERTY()
	ACell* cell_;

	UPROPERTY()
	bool desaturated_;

	UPROPERTY()
	uint8 team_;
};

USTRUCT()
struct FReplayAnimation
{
	GENERATED_BODY()

	UPROPERTY()
	EAnimationType type_;

	UPROPERTY()
	FVector from_;
	UPROPERTY()
	FVector to_;

	UPROPERTY()
	ASomething* a_;
	UPROPERTY()
	ASomething* b_;

	UPROPERTY()
	float duration_;

	UPROPERTY()
	int32 value_ = 0;

	UPROPERTY()
	TArray<FCellChange> cell_changes_;
	UPROPERTY()
	TArray<FSomethingChange> something_changes_;
};

UCLASS()
class SOME_GAME_API AReplayer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AReplayer();

	void AddAnimationToReplay(EAnimationType type, const FVector& from, const FVector& to, ASomething* a, ASomething* b, float duration, int32 value = 0);

	void AddCellChange(ACell* cell, bool desaturated, uint8 team);

	void AddSomethingChange(ASomething* something, bool hidden, uint8 team);

	void ReplayFromIndex(int32 index, uint8 team);

	uint32 GetReplayStackSize() const;

	bool Replaying() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TArray<FReplayAnimation> replay_animations_stack_;

	UPROPERTY()
	TArray<FCellChange> current_cell_changes_;
	UPROPERTY()
	TArray<FSomethingChange> current_something_changes_;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;

	UPROPERTY()
	int32 current_index_ = 0;
	UPROPERTY()
	float wait_ = 0.f;
	UPROPERTY()
	uint8 team_ = 0;
	UPROPERTY()
	bool replaying_ = false;

	UPROPERTY()
	TArray<FCellChange> end_cell_changes_;
	UPROPERTY()
	TArray<FSomethingChange> end_something_changes_;

	UPROPERTY()
	TArray<bool> replayed_commander_destruction_for_team_;

public:	
	// Called every frame
	virtual void Tick(float delta_time) override;

};
