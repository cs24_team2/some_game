// cs24_team2 (c) BY

#pragma once

#include "Building.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasicFactory.generated.h"

class USomethingFactoryWidget;
class AUnit;

UCLASS(Abstract)
class SOME_GAME_API ABasicFactory : public ABuilding
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasicFactory();

	AUnit* CreateUnit(USomethingFactoryWidget* factory);

	TArray<AUnit*>& GetCreatedUnits();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	TArray<AUnit*> created_units_;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
