// cs24_team2 (c) BY

#pragma once

#include "Unit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AerialUnit.generated.h"

UCLASS(Abstract)
class SOME_GAME_API AAerialUnit : public AUnit
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAerialUnit();

	virtual TArray<ACell*> GetMoveToCells() const override;

	virtual bool CanMoveTo(ACell* cell) const override;

	virtual void Destroy(bool animate = true) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void MoveToPrimitive(ACell* cell) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
