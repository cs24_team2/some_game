// cs24_team2 (c) BY

#pragma once

#include "Resources.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SomethingFactoryWidget.generated.h"

class UButton;
class ASomething;
class AGameplayGameMode;
class ACell;

UCLASS(Blueprintable)
class SOME_GAME_API USomethingFactoryWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	ASomething* Create(const uint8 team);

	AGameplayGameMode* GetGameMode();

	bool CanBeCreatedOnCell(ACell* cell) const;

	bool IsEnoughResources(const FResources& resources) const;

	FResources GetResources() const;

	UFUNCTION(BlueprintCallable, Category = "SomethingFactoryWidget")
	bool CanBeCreated();

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UButton* spawn_button_ = nullptr;

	UPROPERTY(EditAnywhere, Category = "SomethingFactoryWidget")
	TSubclassOf<ASomething> created_class_ = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Resources")
	int32 required_H  = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Resources")
	int32 required_C  = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Resources")
	int32 required_Ae = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool water_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool glacier_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool raised_glacier_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool snowy_plains_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool snowy_mountains_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool plains_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool mountains_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool wasteland_allowed = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SomethingFactoryWidget|Allowed Cells")
	bool desert_allowed = false;

	UPROPERTY()
	AGameplayGameMode* game_mode_ = nullptr;
};
