﻿// cs24_team2 (c) BY

#pragma once

#include "GroundUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Builder.generated.h"

class USomethingFactoryWidget;
class ABuilding;

UCLASS(Abstract)
class SOME_GAME_API ABuilder : public AGroundUnit
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABuilder();

	ABuilding* CreateBuilding(USomethingFactoryWidget* factory);

	UFUNCTION(BlueprintCallable, Category = "Builder")
	bool CanGetResources() const;

	UFUNCTION(BlueprintCallable, Category = "Builder")
	void GetResources();

	UFUNCTION(BlueprintCallable, Category = "Builder")
	bool CanDestroyBuilding() const;

	UFUNCTION(BlueprintCallable, Category = "Builder")
	void DestroyBuilding();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
