// cs24_team2 (c) BY

#pragma once

#include "GroundUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Mechanic.generated.h"

UCLASS()
class SOME_GAME_API AMechanic final: public AGroundUnit
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMechanic();

	virtual void MoveTo(ACell* cell) override;

	virtual bool CanAttack(ASomething* target) const override;

	virtual int32 Attack(ASomething* target) override;

	virtual int32 Damage(int32 damage) override;

	virtual int32 Heal(int32 heal_amount) override;

	virtual bool IsVisible(ACell* cell, uint32 turn) const override;

	virtual TArray<ACell*> GetVisibleCells(uint32 turn) const override;

	virtual int32 GET_MAX_MOBILITY() const override;

	virtual int32 GET_MAX_ATTACK_COUNT() const override;

	virtual int32 GET_ATTACK_DAMAGE() const override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 HEAL_AMOUNT = 30;
	UPROPERTY()
	int32 ATTACK_DAMAGE = -HEAL_AMOUNT;
	UPROPERTY()
	int32 MAX_ATTACK_COUNT = 1;
	UPROPERTY()
	int32 MAX_HEALTH = 30;
	UPROPERTY()
	int32 MAX_MOBILITY = 1;
	UPROPERTY()
	int32 ATTACK_DISTANCE = 1;
	UPROPERTY()
	int32 VISIBLE_DISTANCE = 1;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
