// cs24_team2 (c) BY

#pragma once

#include "Camera/CameraComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CameraPlayerController.generated.h"


UCLASS()
class SOME_GAME_API ACameraPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ACameraPlayerController();

	void OnLeftClick();

protected:
	virtual void SetupInputComponent() override;
};
