// cs24_team2 (c) BY

#pragma once

#include "GroundUnit.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Salvo.generated.h"

UCLASS()
class SOME_GAME_API ASalvo : public AGroundUnit
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASalvo();

	virtual int32 GET_MAX_MOBILITY() const override;

	virtual int32 GET_MAX_ATTACK_COUNT() const override;

	virtual int32 GET_ATTACK_DAMAGE() const override;

	virtual int32 GET_MAX_HEALTH() const override;

	virtual int32 GET_VISIBILITY_DISTANCE() const override;

	virtual int32 GET_ATTACK_DISTANCE() const override;

	virtual FString GET_NAME() const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 ATTACK_DAMAGE = 10;
	UPROPERTY()
	int32 MAX_ATTACK_COUNT = 3;
	UPROPERTY()
	int32 MAX_HEALTH = 15;
	UPROPERTY()
	int32 MAX_MOBILITY = 1;
	UPROPERTY()
	int32 ATTACK_DISTANCE = 3;
	UPROPERTY()
	int32 VISIBLE_DISTANCE = 3;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
