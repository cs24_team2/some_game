// cs24_team2 (c) BY

#pragma once

#include "Something.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Unit.generated.h"

class ACell;

UCLASS(Abstract)
class SOME_GAME_API AUnit : public ASomething
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUnit();

	UFUNCTION(BlueprintCallable, Category = "Unit")
	int32 GetMobility() const;

	void ResetMobility();

	UFUNCTION(BlueprintCallable, Category = "Unit")
	int32 GetAttackCount() const;

	void ResetAttackCount();

	virtual TArray<ACell*> GetMoveToCells() const PURE_VIRTUAL(AUnit::GetMoveToCells, return {};);

	TArray<ASomething*> GetAttackableSomethings() const;

	virtual bool CanMoveTo(ACell* cell) const PURE_VIRTUAL(AUnit::CanMoveTo, return false;);

	virtual void MoveTo(ACell* cell);

	virtual bool CanAttack(ASomething* target) const;

	virtual int32 Attack(ASomething* target);

	UFUNCTION(BlueprintCallable, Category = "Unit")
	virtual int32 GET_MAX_MOBILITY() const PURE_VIRTUAL(AUnit::GET_MAX_MOBILITY, return 0;);

	UFUNCTION(BlueprintCallable, Category = "Unit")
	virtual int32 GET_MAX_ATTACK_COUNT() const PURE_VIRTUAL(AUnit::GET_MAX_ATTACK_COUNT, return 0;);

	UFUNCTION(BlueprintCallable, Category = "Unit")
	virtual int32 GET_ATTACK_DAMAGE() const PURE_VIRTUAL(AUnit::GET_ATTACK_DAMAGE, return 0;);

	UFUNCTION(BlueprintCallable, Category = "Unit")
	virtual int32 GET_ATTACK_DISTANCE() const PURE_VIRTUAL(AUnit::GET_ATTACK_DISTANCE, return 0;);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 AttackPrimitive(ASomething* target);

	bool CanAttackPrimitive(const ASomething* target) const;

	virtual void MoveToPrimitive(ACell* cell) PURE_VIRTUAL(AUnit::MoveToPrimitive, );

	UPROPERTY()
	int32 mobility_ = 0;
	UPROPERTY()
	int32 attack_count_ = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
