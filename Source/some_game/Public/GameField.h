// cs24_team2 (c) BY

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameField.generated.h"

class ACell;

UCLASS()
class SOME_GAME_API AGameField final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameField();

	void Generate(int32 height, int32 width, double scale);

	ACell* GetCell(int32 x, int32 y) const;

	const TArray<ACell*>& GetAllCells() const;

	bool AreNeighbours(ACell* cell_a, ACell* cell_b) const;

	TArray<ACell*> GetNeighbours(ACell* cell) const;

	int32 GetDistance(ACell* cell_a, ACell* cell_b) const;

	TArray<ACell*> GetCellsAtDistance(ACell* cell, int32 distance) const;

	void PropogateNextTurn() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	inline ACell* Field(int32 i, int32 j) const;

	UPROPERTY()
	TArray<ACell*> field_;
	UPROPERTY()
	int32 width_ = 0;
	UPROPERTY()
	int32 height_ = 0;

	UPROPERTY()
	TArray<UStaticMesh*> meshes_;

	UPROPERTY()
	UMaterialInstance* highlight_material_ = nullptr;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
