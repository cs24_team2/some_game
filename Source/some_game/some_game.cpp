// Copyright Epic Games, Inc. All Rights Reserved.

#include "some_game.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, some_game, "some_game" );
