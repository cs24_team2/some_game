// cs24_team2 (c) BY


#include "CameraPlayerController.h"

ACameraPlayerController::ACameraPlayerController() : APlayerController()
{
	bShowMouseCursor = true;
}

void ACameraPlayerController::OnLeftClick()
{
	FHitResult hit_result;
	GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, hit_result);

	AActor* actor = hit_result.GetActor();
	
	if (actor) {
		actor->NotifyActorOnClicked(EKeys::LeftMouseButton);
	}
}

void ACameraPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction(TEXT("LeftClick"), IE_Pressed, this, &ACameraPlayerController::OnLeftClick);
}
