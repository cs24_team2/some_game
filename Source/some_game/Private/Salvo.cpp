// cs24_team2 (c) BY


#include "Salvo.h"

#include "GameField.h"
#include "GameplayGameMode.h"

// Sets default values
ASalvo::ASalvo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/salvo.salvo'")));
}

int32 ASalvo::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 ASalvo::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 ASalvo::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 ASalvo::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

int32 ASalvo::GET_VISIBILITY_DISTANCE() const
{
	return VISIBLE_DISTANCE;
}

int32 ASalvo::GET_ATTACK_DISTANCE() const
{
	return ATTACK_DISTANCE;
}

FString ASalvo::GET_NAME() const
{
	return TEXT("Salvo");
}

// Called when the game starts or when spawned
void ASalvo::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASalvo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

