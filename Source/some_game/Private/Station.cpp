// cs24_team2 (c) BY


#include "Station.h"

#include "Cell.h"

// Sets default values
AStation::AStation()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/BuildingMeshes/station.station'")));
}

FResources AStation::GetResources() const
{
	switch (GetCell(current_turn_)->type_) {
	case CellType::GLACIER:
		return {3, 0, 0};
	case CellType::SNOWY_PLAINS:
		return {1, 2, 0};
	case CellType::PLAINS:
		return {0, 3, 0};
	case CellType::WASTELAND:
		return {0, 2, 1};
	case CellType::DESERT:
		return {0, 0, 3};
	default:
		return {0, 0, 0};
	}
}

int32 AStation::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 AStation::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

int32 AStation::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString AStation::GET_NAME() const
{
	return TEXT("Station");
}

// Called when the game starts or when spawned
void AStation::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

