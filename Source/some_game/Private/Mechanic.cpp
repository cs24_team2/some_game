// cs24_team2 (c) BY


#include "Mechanic.h"

#include "AerialUnit.h"
#include "Animator.h"
#include "GameplayGameMode.h"
#include "GameField.h"

// Sets default values
AMechanic::AMechanic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/mechanic.mechanic'")));
}

void AMechanic::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AMechanic::CanAttack(ASomething* target) const {
	if (Cast<AAerialUnit>(target) != nullptr) {
		return false;
	}
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	if (target->GetHealth(current_turn_) >= target->GET_MAX_HEALTH()) {
		return false;
	}

	return team_ == target->GetTeam();
}

int32 AMechanic::Attack(ASomething* target) {
	--attack_count_;
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	last_animation_id_ = game_mode_->GetAnimator()->AnimateHeal(this, target, HEAL_AMOUNT);
	return target->Heal(HEAL_AMOUNT);
}

int32 AMechanic::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AMechanic::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AMechanic::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AMechanic::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AMechanic::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AMechanic::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AMechanic::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AMechanic::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AMechanic::GET_NAME() const
{
	return TEXT("Mechanic");
}

// Called when the game starts or when spawned
void AMechanic::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMechanic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

