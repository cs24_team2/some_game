// cs24_team2 (c) BY


#include "Cell.h"

#include "Building.h"
#include "GroundUnit.h"
#include "AerialUnit.h"
#include "GameplayGameMode.h"
#include <Kismet/GameplayStatics.h>

#include "Replayer.h"

// Sets default values
ACell::ACell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	UStaticMeshComponent* mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	mesh->SetupAttachment(GetRootComponent());
	mesh->SetMobility(EComponentMobility::Movable);
	mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	meshes_.Add(mesh);

	UStaticMeshComponent* left_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("left_mesh"));
	left_mesh->SetupAttachment(mesh);
	left_mesh->SetMobility(EComponentMobility::Movable);
	left_mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	meshes_.Add(left_mesh);

	UStaticMeshComponent* right_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("right_mesh"));
	right_mesh->SetupAttachment(mesh);
	right_mesh->SetMobility(EComponentMobility::Movable);
	right_mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	meshes_.Add(right_mesh);
}

void ACell::PropogateNextTurn()
{
	ABuilding* building = occupying_building_.Last();
	AGroundUnit* ground_unit = occupying_ground_unit_.Last();
	AAerialUnit* aerial_unit = occupying_aerial_unit_.Last();
	occupying_building_.Add(building);
	occupying_ground_unit_.Add(ground_unit);
	occupying_aerial_unit_.Add(aerial_unit);
	if (building) {
		building->NextTurn();
	}
	if (ground_unit) {
		ground_unit->NextTurn();
		ground_unit->ResetMobility();
		ground_unit->ResetAttackCount();
	}
	if (aerial_unit) {
		aerial_unit->NextTurn();
		aerial_unit->ResetMobility();
		aerial_unit->ResetAttackCount();
	}

	const uint8 team_count = game_mode_->GetTeamCount();
	for (uint8 i = 0; i < team_count; ++i) {
		uint32 t = viewer_counts_.Last(team_count - 1);
		viewer_counts_.Add(t);
	}
}

uint32& ACell::ViewerCounts(uint8 team, uint32 turn)
{
	return viewer_counts_[turn * game_mode_->GetTeamCount() + team];
}

void ACell::ModifyViewerCounts(uint8 team, uint32 turn, int32 value)
{
	ViewerCounts(team, turn) += value;
	discovered_[team] = true;
	game_mode_->UpdateCellMeshes(this, turn);

	// for replay
	AReplayer* replayer = game_mode_->GetReplayer();
	if (ViewerCounts(team, turn)) {
		replayer->AddCellChange(this, false, team);
		if (ABuilding* ptr = occupying_building_[turn]) {
			replayer->AddSomethingChange(ptr, false, team);
		}
		if (AAerialUnit* ptr = occupying_aerial_unit_[turn]) {
			replayer->AddSomethingChange(ptr, false, team);
		}
		if (AGroundUnit* ptr = occupying_ground_unit_[turn]) {
			replayer->AddSomethingChange(ptr, false, team);
		}
		return;
	}
	// set all meshes invisible
	if (ABuilding* ptr = occupying_building_[turn]) {
		replayer->AddSomethingChange(ptr, true, team);
	}
	if (AAerialUnit* ptr = occupying_aerial_unit_[turn]) {
		replayer->AddSomethingChange(ptr, true, team);
	}
	if (AGroundUnit* ptr = occupying_ground_unit_[turn]) {
		replayer->AddSomethingChange(ptr, true, team);
	}

	replayer->AddCellChange(this, true, team);
}

void ACell::SetMaterialDesaturated(const bool desaturated)
{
	const float val = desaturated;
	for (UMaterialInstanceDynamic* mat : materials_) {
		mat->SetScalarParameterValue("Desaturate", val);
	}
}

void ACell::SetCellHidden(bool hidden)
{
	for (UStaticMeshComponent* mesh : meshes_) {
		mesh->SetHiddenInGame(hidden);
		mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, hidden ? ECollisionResponse::ECR_Ignore : ECollisionResponse::ECR_Block);
	}
	hidden_ = hidden;
}

void ACell::Highlight(bool highlight)
{
	for (UStaticMeshComponent* mesh : meshes_) {
		mesh->SetOverlayMaterial(highlight ? highlight_overlay_material_ : nullptr);
	}
}

void ACell::NotifyActorOnClicked(FKey ButtonPressed)
{
	if (!hidden_ && ButtonPressed == EKeys::LeftMouseButton) {
		game_mode_->ClickedOnCell(this);
	}
}

// Called when the game starts or when spawned
void ACell::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ACell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

