// cs24_team2 (c) BY


#include "Building.h"

#include "Cell.h"
#include "GameField.h"
#include "GameplayGameMode.h"
#include "Replayer.h"

// Sets default values
ABuilding::ABuilding()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

bool ABuilding::IsVisible(ACell* cell, uint32 turn) const
{
	return cell && cell == GetCell(turn);
}

TArray<ACell*> ABuilding::GetVisibleCells(uint32 turn) const
{
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetNeighbours(GetCell(turn));
	result.Add(GetCell(turn));
	return result;
}

void ABuilding::Destroy(const bool animate)
{
	GetCell(current_turn_)->occupying_building_[current_turn_] = nullptr;
	ModifyVisibleCellViewerCounts(current_turn_, -1);
	destruction_turn_ = current_turn_;
	// game_mode_->UpdateSomethingVisibility(this, current_turn_);

	if (animate) {
		// for replay
		const int32 team_count = game_mode_->GetTeamCount();
		AReplayer* replayer = game_mode_->GetReplayer();
		for (int32 i = 0; i < team_count; ++i)
			replayer->AddSomethingChange(this, true, i);

		// animation
		game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
		last_animation_id_ = game_mode_->GetAnimator()->AnimateDestroy(this);
	}
}

// Called when the game starts or when spawned
void ABuilding::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
