// cs24_team2 (c) BY


#include "GameField.h"

#include <Kismet/GameplayStatics.h>

#include "Cell.h"
#include "GameplayGameMode.h"

// Sets default values
AGameField::AGameField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

    static ConstructorHelpers::FObjectFinder<UStaticMesh> water_mesh    (TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Water/water.water'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> glacier_mesh  (TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Glacier/glacier.glacier'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> raised_glacier_mesh(TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/RaisedGlacier/raised_glacier.raised_glacier'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> snowy_plains_mesh(TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/SnowyPlains/snowy_plains.snowy_plains'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> snowy_mountains_mesh(TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/SnowyMountain/snowy_mountain.snowy_mountain'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> plains_mesh(TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Plains/plains.plains'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> mountains_mesh(TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Mountain/mountain.mountain'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> wasteland_mesh   (TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Wasteland/wasteland.wasteland'"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> desert_mesh   (TEXT("/Script/Engine.StaticMesh'/Game/HexMeshes/Desert/desert.desert'"));

    meshes_.Add(water_mesh.Succeeded()           ? water_mesh.Object           : nullptr);
    meshes_.Add(glacier_mesh.Succeeded()         ? glacier_mesh.Object         : nullptr);
    meshes_.Add(raised_glacier_mesh.Succeeded()  ? raised_glacier_mesh.Object  : nullptr);
    meshes_.Add(snowy_plains_mesh.Succeeded()    ? snowy_plains_mesh.Object    : nullptr);
    meshes_.Add(snowy_mountains_mesh.Succeeded() ? snowy_mountains_mesh.Object : nullptr);
    meshes_.Add(plains_mesh.Succeeded()          ? plains_mesh.Object          : nullptr);
    meshes_.Add(mountains_mesh.Succeeded()       ? mountains_mesh.Object       : nullptr);
    meshes_.Add(wasteland_mesh.Succeeded()       ? wasteland_mesh.Object       : nullptr);
    meshes_.Add(desert_mesh.Succeeded()          ? desert_mesh.Object          : nullptr);

    static ConstructorHelpers::FObjectFinder<UMaterialInstance> highlight_material(TEXT("/Script/Engine.MaterialInstanceConstant'/Game/Materials/TranslucentCellOverlay.TranslucentCellOverlay'"));

    highlight_material_ = highlight_material.Succeeded() ? highlight_material.Object : nullptr;
}

void AGameField::Generate(int32 height, int32 width, double scale)
{
    constexpr double GLOBAL_HEIGHT_PERLIN_SCALE = 5.187349124;
    constexpr double LOCAL_HEIGHT_PERLIN_SCALE = 1.82948024;
    constexpr double TEMP_PERLIN_SCALE = 2.78427849;
    // constexpr double WIND_PERLIN_SCALE = 2.109204902;


    height_ = height;
    width_ = width;

    AGameplayGameMode* game_mode = (AGameplayGameMode*)(UGameplayStatics::GetGameMode(this));

    FActorSpawnParameters params = FActorSpawnParameters();
    params.Owner = this;
    params.bNoFail = true;
    params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    field_.Reserve(height * width);
    for (int32 i = 0; i < height * width; ++i) {
        field_.Add(GetWorld()->SpawnActor<ACell>(params));
        field_[i]->game_mode_ = game_mode;

        uint8 team_count = game_mode->GetTeamCount();
        field_[i]->discovered_.Init(false, team_count);
        field_[i]->viewer_counts_.Init(0, team_count);
    }

    constexpr double sqrt2 = UE_SQRT_2;
    constexpr double sqrt3 = UE_SQRT_3;
    for (int32 i = 0; i < height; ++i) {
        for (int32 j = 0; j < width; ++j) {
            ACell* cell = Field(i, j);
            cell->x_ = 2 * j + i % 2;
            cell->y_ = i;

            cell->SetActorLocation(FVector(StaticCast<double>(cell->x_) * scale, sqrt3 * StaticCast<double>(cell->y_) * scale, 0.));
            //cell->SetActorRotation(FRotator(0., 90., 0.));
        }
    }

    bool retry = true;
    while (retry) {
        retry = false;
        for (int32 i = 0; i < height; ++i) {
            for (int32 j = 0; j < width; ++j) {
                Field(i, j)->type_ = CellType::UNDEFINED;
            }
        }

        auto gen_perlin_vectors = [](const int32 perlin_height, const int32 perlin_width, TArray<TArray<TStaticArray<double, 2>>>& vectors)
            {
                vectors.Reserve(perlin_height);
                for (int32 i = 0; i < perlin_height; ++i) {
                    vectors.Emplace();
                    vectors[i].Reserve(perlin_width);
                    for (int32 j = 0; j < perlin_width; ++j) {
                        const double x = FMath::FRandRange(-1., 1.), y = FMath::FRandRange(-1., 1.);
                        const double r = FGenericPlatformMath::Sqrt(x * x + y * y);
                        TStaticArray<double, 2> vec;
                        vec[0] = x / r;
                        vec[1] = y / r;
                        vectors[i].Add(vec);
                    }
                }
            };

        const int32 global_height_perlin_height = StaticCast<int32>(StaticCast<double>(height) * sqrt3 / GLOBAL_HEIGHT_PERLIN_SCALE) + 3;
        const int32 global_height_perlin_width = StaticCast<int32>(StaticCast<double>(2 * width) / GLOBAL_HEIGHT_PERLIN_SCALE) + 3;
        TArray<TArray<TStaticArray<double, 2>>> global_height_perlin_vectors;
        gen_perlin_vectors(global_height_perlin_height, global_height_perlin_width, global_height_perlin_vectors);

        const int32 local_height_perlin_height = StaticCast<int32>(StaticCast<double>(height) * sqrt3 / LOCAL_HEIGHT_PERLIN_SCALE) + 3;
        const int32 local_height_perlin_width = StaticCast<int32>(StaticCast<double>(2 * width) / LOCAL_HEIGHT_PERLIN_SCALE) + 3;
        TArray<TArray<TStaticArray<double, 2>>> local_height_perlin_vectors;
        gen_perlin_vectors(local_height_perlin_height, local_height_perlin_width, local_height_perlin_vectors);

        const int32 temp_perlin_height = StaticCast<int32>(StaticCast<double>(height) * sqrt3 / TEMP_PERLIN_SCALE) + 3;
        const int32 temp_perlin_width = StaticCast<int32>(StaticCast<double>(2 * width) / TEMP_PERLIN_SCALE) + 3;
        TArray<TArray<TStaticArray<double, 2>>> temp_perlin_vectors;
        gen_perlin_vectors(temp_perlin_height, temp_perlin_width, temp_perlin_vectors);

        // const int32 wind_perlin_height = StaticCast<int32>(StaticCast<double>(height) * sqrt3 / WIND_PERLIN_SCALE) + 3;
        // const int32 wind_perlin_width = StaticCast<int32>(StaticCast<double>(2 * width) / WIND_PERLIN_SCALE) + 3;
        // TArray<TArray<TStaticArray<double, 2>>> wind_perlin_vectors;
        // gen_perlin_vectors(wind_perlin_height, wind_perlin_width, wind_perlin_vectors);


        auto serp = [](const double a0, const double a1, const double w)
	        {
		        return (a1 - a0) * ((w * (w * 6. - 15.) + 10.) * w * w * w) + a0;
	        };

        auto dot_to = [](const int32 ix, const int32 iy, const double x, const double y, const TArray<TArray<TStaticArray<double, 2>>>& perlin_vectors)
	        {
		        return (x - ix) * perlin_vectors[iy][ix][0] + (y - iy) * perlin_vectors[iy][ix][1];
	        };

        auto perlin = [&](double x, double y, const TArray<TArray<TStaticArray<double, 2>>>& perlin_vectors)
	        {
		        const int32 x0 = StaticCast<int32>(x), y0 = StaticCast<int32>(y), x1 = x0 + 1, y1 = y0 + 1;
		        const double sx = x - x0, sy = y - y0;
		        return serp(serp(dot_to(x0, y0, x, y, perlin_vectors), dot_to(x1, y0, x, y, perlin_vectors), sx),
		                    serp(dot_to(x0, y1, x, y, perlin_vectors), dot_to(x1, y1, x, y, perlin_vectors), sx),
		                    sy);
	        };

        auto normalized_perlin = [&](const double x, const double y, const TArray<TArray<TStaticArray<double, 2>>>& perlin_vectors)
	        {
		        const double v = perlin(x, y, perlin_vectors);
		        return ((12. * v * v - 20.) * v * v + 15.) * v * sqrt2 * .125;
	        };

        auto perlin_at_point = [&](const double x, const double y, const double perlin_scale, const TArray<TArray<TStaticArray<double, 2>>>& perlin_vectors)
            {
                return normalized_perlin((x + 1.) / perlin_scale, (y + 1.) / perlin_scale, perlin_vectors);
            };

        auto static_temp = [](double x)
    		{
	            x = .5 - FGenericPlatformMath::Abs(x - .5);
	            return ((384. * x - 480.) * x + 160.) * x * x * x - 1.;
            };

        auto calc_height = [&](const double x, const double y)
            {
                return perlin_at_point(x, y, GLOBAL_HEIGHT_PERLIN_SCALE, global_height_perlin_vectors) * .75 + perlin_at_point(x, y, LOCAL_HEIGHT_PERLIN_SCALE, local_height_perlin_vectors) * .25;
            };

        auto calc_temp = [&](const double x, const double y)
    		{
				return static_temp(y / sqrt3 / (height - 1)) * .75 + perlin_at_point(x, y, TEMP_PERLIN_SCALE, temp_perlin_vectors) * .25;
            };

        // auto calc_wind = [&](const double x, const double y)
        //     {
        //         return perlin_at_point(x, y, WIND_PERLIN_SCALE, wind_perlin_vectors);
        //     };

        constexpr int32 CELL_TYPES_COUNT = int32(CellType::UNDEFINED);
        int32 undecided_count = width * height;
        TArray<TArray<TStaticArray<double, CELL_TYPES_COUNT>>> probabilities;
        TArray<TArray<double>> max, sum;
        probabilities.Reserve(height);
        max.Reserve(height);
        sum.Reserve(height);
        for (int32 i = 0; i < height; ++i) {
            probabilities.Emplace();
            max.Emplace();
            sum.Emplace();
            probabilities[i].Reserve(width);
            max[i].Reserve(width);
            sum[i].Reserve(width);
            for (int32 j = 0; j < width; ++j) {
                const double x = 2 * j + i % 2, y = i * sqrt3;
                const double h = calc_height(x, y), t = calc_temp(x, y);

                TStaticArray<double, CELL_TYPES_COUNT> prob;
                prob[int32(CellType::WATER)] = .125 * (1. - h) * (4. - (1. - t) * (1. - t)) * .5;
                prob[int32(CellType::GLACIER)] = .1 * (1. - h) * (1. - t) * (1. - t);
                prob[int32(CellType::RAISED_GLACIER)] = .0625 * (1. - h) * (1. - h) * (1. - t) * (1. - t);
                prob[int32(CellType::SNOWY_PLAINS)] = .25 * (1. - h * h) * (1. - h * h) * (1. - t) * (1. - t);
                prob[int32(CellType::SNOWY_MOUNTAINS)] = .0625 * (1. + h) * (1. + h) * (1. - t) * (1. - t);
                prob[int32(CellType::PLAINS)] = (1. - h * h) * (1. - h * h) * (1. - t * t) * (1. - t * t) * 1.5;
                prob[int32(CellType::MOUNTAINS)] = .0625 * (1. + h) * (1. + h) * (4. - (1. - t) * (1. - t));
                const double temp = 1. - (t - .5) * (t - .5) / 2.25;
                prob[int32(CellType::WASTELAND)] = (1. - h * h) * (1. - h * h) * temp * temp * temp * temp;
                prob[int32(CellType::DESERT)] = .25 * (1. - h * h) * (1. - h * h) * (1. + t) * (1. + t);

                probabilities[i].Add(prob);
                double p_max = 0., p_sum = 0.;
                for (int32 k = 0; k < CELL_TYPES_COUNT; ++k) {
                    p_max = FGenericPlatformMath::Max(p_max, prob[k]);
                    p_sum += prob[k];
                }
                max[i].Add(p_max);
                sum[i].Add(p_sum);
            }
        }

        TStaticArray<double, CELL_TYPES_COUNT> zero_probabilities(InPlace, 0.);

        auto calc_entropy = [&](int32 i, int32 j)
            {
                return max[i][j] / sum[i][j];
            };

        auto choose_random_type = [&](int32 i, int32 j)
            {
                TStaticArray<double, CELL_TYPES_COUNT>& p = probabilities[i][j];
                const double value = FMath::FRandRange(0., sum[i][j]);
                double acc = 0.;
                for (int32 k = 0; k < CELL_TYPES_COUNT; ++k) {
                    acc += p[k];
                    if (acc > value) {
                        return k;
                    }
                }
                return CELL_TYPES_COUNT - 1;
            };

        TArray<TArray<int32>> allowed_neighbours;
        allowed_neighbours.Reserve(CELL_TYPES_COUNT);
        for (int32 i = 0; i < CELL_TYPES_COUNT; ++i) {
            allowed_neighbours.Emplace();
        }
        allowed_neighbours[int32(CellType::WATER)] = { int32(CellType::WATER), int32(CellType::GLACIER), int32(CellType::RAISED_GLACIER), int32(CellType::SNOWY_PLAINS), int32(CellType::SNOWY_MOUNTAINS), int32(CellType::PLAINS), int32(CellType::MOUNTAINS), int32(CellType::WASTELAND), int32(CellType::DESERT) };
        allowed_neighbours[int32(CellType::GLACIER)] = { int32(CellType::WATER), int32(CellType::GLACIER), int32(CellType::RAISED_GLACIER), int32(CellType::SNOWY_PLAINS), int32(CellType::SNOWY_MOUNTAINS) };
    	allowed_neighbours[int32(CellType::RAISED_GLACIER)] = { int32(CellType::WATER), int32(CellType::GLACIER), int32(CellType::RAISED_GLACIER), int32(CellType::SNOWY_PLAINS), int32(CellType::SNOWY_MOUNTAINS) };
        allowed_neighbours[int32(CellType::SNOWY_PLAINS)] = { int32(CellType::WATER), int32(CellType::GLACIER), int32(CellType::RAISED_GLACIER), int32(CellType::SNOWY_PLAINS), int32(CellType::SNOWY_MOUNTAINS), int32(CellType::PLAINS), int32(CellType::MOUNTAINS) };
        allowed_neighbours[int32(CellType::SNOWY_MOUNTAINS)] = { int32(CellType::WATER), int32(CellType::GLACIER), int32(CellType::RAISED_GLACIER), int32(CellType::SNOWY_PLAINS), int32(CellType::SNOWY_MOUNTAINS), int32(CellType::PLAINS), int32(CellType::MOUNTAINS) };
        allowed_neighbours[int32(CellType::PLAINS)] = { int32(CellType::WATER), int32(CellType::SNOWY_MOUNTAINS), int32(CellType::PLAINS), int32(CellType::MOUNTAINS), int32(CellType::WASTELAND) };
        allowed_neighbours[int32(CellType::MOUNTAINS)] = { int32(CellType::WATER), int32(CellType::SNOWY_MOUNTAINS), int32(CellType::PLAINS), int32(CellType::MOUNTAINS), int32(CellType::WASTELAND) };
        allowed_neighbours[int32(CellType::WASTELAND)] = { int32(CellType::WATER), int32(CellType::PLAINS), int32(CellType::MOUNTAINS), int32(CellType::WASTELAND), int32(CellType::DESERT) };
        allowed_neighbours[int32(CellType::DESERT)] = { int32(CellType::WATER), int32(CellType::MOUNTAINS), int32(CellType::WASTELAND), int32(CellType::DESERT) };

        auto update_probabilities = [&](ACell* cell, int32 i, int32 j)
    		{
                TStaticArray<double, CELL_TYPES_COUNT>& p = probabilities[i][j];
                bool changed_something = false;
				for (int32 k = 0; k < CELL_TYPES_COUNT; ++k) {
					if (p[k] > 0.) {
						for (const ACell* neighbour : GetNeighbours(cell)) {
                            const int32 nj = neighbour->x_ / 2, ni = neighbour->y_;
                            TStaticArray<double, CELL_TYPES_COUNT>& np = probabilities[ni][nj];
                            bool has_allowed = false;
							for (const int32 allowed_type : allowed_neighbours[k]) {
								if (np[allowed_type] > 0.) {
                                    has_allowed = true;
                                    break;
								}
							}
                            if (!has_allowed) {
                                p[k] = 0.;
                                changed_something = true;
                                break;
                            }
						}
					}
				}
				if (changed_something) {
                    double p_max = 0., p_sum = 0.;
                    for (int32 k = 0; k < CELL_TYPES_COUNT; ++k) {
                        p_max = FGenericPlatformMath::Max(p_max, p[k]);
                        p_sum += p[k];
                    }
                    max[i][j] = p_max;
                    sum[i][j] = p_sum;
				}
                return changed_something;
            };


        while (undecided_count) {
            int32 max_idx_i = -1, max_idx_j = -1;
            double max_entropy = 0.;
            for (int32 i = 0; i < height; ++i) {
                for (int32 j = 0; j < width; ++j) {
                    if (Field(i, j)->type_ == CellType::UNDEFINED) {
                        if (const double ent = calc_entropy(i, j); ent > max_entropy) {
                            max_entropy = ent;
                            max_idx_i = i;
                            max_idx_j = j;
                        }
                    }
                }
            }
            int32 type = choose_random_type(max_idx_i, max_idx_j);
            probabilities[max_idx_i][max_idx_j] = zero_probabilities;
            probabilities[max_idx_i][max_idx_j][type] = 1.;
            max[max_idx_i][max_idx_j] = 1.;
            sum[max_idx_i][max_idx_j] = 1.;
            --undecided_count;
            Field(max_idx_i, max_idx_j)->type_ = CellType(type);
            TQueue<ACell*> q;
            q.Enqueue(Field(max_idx_i, max_idx_j));
            while (!q.IsEmpty()) {
                ACell* c;
                q.Dequeue(c);
                for (ACell* neighbour : GetNeighbours(c)) {
                    int32 j = neighbour->x_ / 2, i = neighbour->y_;
                    if (Field(i, j)->type_ != CellType::UNDEFINED) {
                        continue;
                    }

                    if (!update_probabilities(neighbour, i, j)) {
	                    continue;
                    }

                    if (max[i][j] == 0.) {
                        retry = true;
                        q.Empty();
                        undecided_count = 0;
                        break;
                    }

                    if (max[i][j] == sum[i][j]) {
                        for (int32 k = 0; k < CELL_TYPES_COUNT; ++k) {
                            if (probabilities[i][j][k] > 0.) {
                                probabilities[i][j] = zero_probabilities;
                                probabilities[i][j][k] = 1.;
                                max[i][j] = 1.;
                                sum[i][j] = 1.;
                                --undecided_count;
                                Field(i, j)->type_ = CellType(k);
                                break;
                            }
                        }
                    }

                    q.Enqueue(neighbour);
                }
            }
        }
    }
    
    double shift = 2. * scale * width;
    TArray<int32> num_of_materials = { 3, 5, 5, 4, 4, 4, 4, 4, 3 };
    for (int32 i = 0; i < height * width; ++i) {
        uint8 idx = StaticCast<uint8>(field_[i]->type_);
        UStaticMesh* cell_mesh = meshes_[idx];
        for (int32 j = 0; j < num_of_materials[idx]; ++j) {
            field_[i]->materials_.Add(UMaterialInstanceDynamic::Create(cell_mesh->GetMaterial(j), field_[i]));
        }
        field_[i]->highlight_overlay_material_ = highlight_material_;
        for (UStaticMeshComponent* mesh : field_[i]->meshes_) {
            mesh->SetStaticMesh(cell_mesh);
            for (int32 j = 0; j < num_of_materials[idx]; ++j) {
                mesh->SetMaterial(j, field_[i]->materials_[j]);
            }
        }
        field_[i]->meshes_[1]->AddWorldOffset(FVector(-shift, 0., 0.));
        field_[i]->meshes_[2]->AddWorldOffset(FVector(shift, 0., 0.));
    }
}

ACell* AGameField::GetCell(int32 x, int32 y) const
{
    if (y < 0 || height_ <= y) {
        return nullptr;
    }

    while (x < 0) {
        x += 2 * width_;
    }
    while (x >= 2 * width_) {
        x -= 2 * width_;
    }
    if (x % 2 != y % 2) {
        return nullptr;
    }
    return Field(y, x / 2);
}

const TArray<ACell*>& AGameField::GetAllCells() const
{
    return field_;
}

bool AGameField::AreNeighbours(ACell* cell_a, ACell* cell_b) const
{
    int32 x_diff = FGenericPlatformMath::Abs(cell_a->x_ - cell_b->x_);
    int32 y_diff = FGenericPlatformMath::Abs(cell_a->y_ - cell_b->y_);

    return y_diff < 2 && (y_diff + x_diff == 2 || x_diff - y_diff == 2 * (width_ - 1));
}

TArray<ACell*> AGameField::GetNeighbours(ACell* cell) const
{
    TArray<ACell*> answer;
    int32 x = cell->x_, y = cell->y_;
    if (ACell* ptr = GetCell(x - 2, y)) {
        answer.Add(ptr);
    }
    if (ACell* ptr = GetCell(x + 2, y)) {
        answer.Add(ptr);
    }
    if (ACell* ptr = GetCell(x - 1, y - 1)) {
        answer.Add(ptr);
    }
    if (ACell* ptr = GetCell(x - 1, y + 1)) {
        answer.Add(ptr);
    }
    if (ACell* ptr = GetCell(x + 1, y - 1)) {
        answer.Add(ptr);
    }
    if (ACell* ptr = GetCell(x + 1, y + 1)) {
        answer.Add(ptr);
    }

    return answer;
}

int32 AGameField::GetDistance(ACell* cell_a, ACell* cell_b) const
{
    int32 ver = FGenericPlatformMath::Abs(cell_a->y_ - cell_b->y_);
    int32 xl = cell_a->x_, xr = cell_b->x_;
    if (xl > xr) {
        Swap(xl, xr);
    }
    int32 hor = FGenericPlatformMath::Min(xr - xl, FGenericPlatformMath::Abs(xr - xl - 2 * width_)) - ver;
    return FGenericPlatformMath::Max(ver, (hor + 1) / 2 + ver);
}

TArray<ACell*> AGameField::GetCellsAtDistance(ACell* cell, int32 distance) const
{
    TArray<ACell*> answer;
    int32 x = cell->x_, y = cell->y_;
    for (int32 x_offset = 1; x_offset <= distance; ++x_offset) {
        answer.Add(GetCell(x - 2 * x_offset, y));
        answer.Add(GetCell(x + 2 * x_offset, y));
    }
    for (int32 y_offset = 1; y_offset <= distance; ++y_offset) {
        for (int32 x_offset = -2 * distance + y_offset; x_offset <= 2 * distance - y_offset; x_offset += 2) {
            if (ACell* ptr = GetCell(x + x_offset, y - y_offset)) {
                answer.Add(ptr);
            }
            if (ACell* ptr = GetCell(x + x_offset, y + y_offset)) {
                answer.Add(ptr);
            }
        }
    }
    return answer;
}

void AGameField::PropogateNextTurn() const
{
    for (int32 i = 0; i < height_ * width_; ++i) {
        field_[i]->PropogateNextTurn();
    }
}

// Called when the game starts or when spawned
void AGameField::BeginPlay()
{
	Super::BeginPlay();
	
}

inline ACell* AGameField::Field(int32 i, int32 j) const
{
    return field_[i * width_ + j];
}

// Called every frame
void AGameField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

