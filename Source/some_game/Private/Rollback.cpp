// cs24_team2 (c) BY


#include "Rollback.h"

#include "GameplayGameMode.h"
#include "GameField.h"
#include "BasicFactory.h"
#include "Cell.h"

// Sets default values
ARollback::ARollback()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/rollback.rollback'")));
}


void ARollback::MoveTo(ACell* cell) {
	accumulated_charge_ = 0;
	MoveToPrimitive(cell);
}

bool ARollback::CanAttack(ASomething* target) const {
	if (accumulated_charge_ < 5) {
		return false;
	}
	if (Cast<ABasicFactory>(target) == nullptr) {
		return false;
	}
	const AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}

	return team_ != target->GetTeam();
}

int32 ARollback::Attack(ASomething* target) {
	accumulated_charge_ = 0;
	for (AUnit* unit : Cast<ABasicFactory>(target)->GetCreatedUnits()) {
		if (unit) {
			unit->Destroy();
		}
	}
	target->Destroy();
	return 0;
}

int32 ARollback::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 ARollback::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool ARollback::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> ARollback::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 ARollback::AccumulateCharge() {
	if (const ACell* cell = GetCell(current_turn_)) {
		const ABasicFactory* factory = Cast<ABasicFactory>(cell->occupying_building_.Last());
		if (factory && factory->GetTeam() != team_) {
			++accumulated_charge_;
		}
		else {
			accumulated_charge_ = 0;
		}
	}
	return accumulated_charge_;
}


int32 ARollback::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 ARollback::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 ARollback::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 ARollback::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString ARollback::GET_NAME() const
{
	return TEXT("Rollback");
}


// Called when the game starts or when spawned
void ARollback::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARollback::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

