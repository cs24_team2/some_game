// cs24_team2 (c) BY


#include "GameplayGameMode.h"

#include <Kismet/GameplayStatics.h>

#include "GameField.h"
#include "Cell.h"
#include "Something.h"
#include "Unit.h"
#include "Building.h"
#include "AerialUnit.h"
#include "Animator.h"
#include "GroundUnit.h"
#include "Commander.h"
#include "CameraPlayerController.h"
#include "CameraPawn.h"
#include "Rollback.h"
#include "SomethingFactoryWidget.h"
#include "BasicFactory.h"
#include "Builder.h"
#include "Groundhog.h"
#include "MyGameInstance.h"
#include "Replayer.h"
#include "Station.h"

AGameplayGameMode::AGameplayGameMode() : AGameModeBase()
{
	PlayerControllerClass = ACameraPlayerController::StaticClass();

	DefaultPawnClass = ACameraPawn::StaticClass();

	static ConstructorHelpers::FObjectFinder<UStaticMesh> default_something_mesh(TEXT("/Script/Engine.StaticMesh'/Engine/BasicShapes/Cone.Cone'"));

	default_something_mesh_ = default_something_mesh.Succeeded() ? default_something_mesh.Object : nullptr;

	static ConstructorHelpers::FObjectFinder<UMaterial> something_highlight_material(TEXT("/Script/Engine.Material'/Game/Materials/TranslucentLinesOverlay.TranslucentLinesOverlay'"));

	something_highlight_material_ = something_highlight_material.Succeeded() ? something_highlight_material.Object : nullptr;
}

AGameField* AGameplayGameMode::GetGameField() const
{
	return game_field_;
}

AAnimator* AGameplayGameMode::GetAnimator() const
{
	return animator_;
}

AReplayer* AGameplayGameMode::GetReplayer() const
{
	return replayer_;
}

uint8 AGameplayGameMode::GetTeamCount() const
{
	return team_count_;
}

int32 AGameplayGameMode::GetDisplayTurn() const
{
	return current_turn_ / team_count_;
}

uint32 AGameplayGameMode::GetCurrentTurn() const
{
	return current_turn_;
}

uint8 AGameplayGameMode::GetCurrentTeam() const
{
	return current_team_;
}

FResources& AGameplayGameMode::GetTeamResources(const uint8 team)
{
	return resources_[team];
}

ASomething* AGameplayGameMode::GetSelectedSomething() const
{
	return selected_something_;
}

const TArray<ASomething*>& AGameplayGameMode::GetSomethingsOfClassAndTeam(const TSubclassOf<ASomething> subclass, const uint8 team)
{
	if (!class_team_somethings_.Contains(subclass)) {
		F2DASomethingTArrayStruct t;
		t._.Init({ {} }, team_count_);
		class_team_somethings_.Add(subclass, t);
	}

	return class_team_somethings_[subclass]._[team]._;
}

UStaticMesh* AGameplayGameMode::GetDefaultSomethingMesh() const
{
	return default_something_mesh_;
}

UMaterial* AGameplayGameMode::GetSomethingHighlightMaterial() const
{
	return something_highlight_material_;
}

FVector4 AGameplayGameMode::GetTeamColor(const uint8 team) const
{
		switch (team) {
		case 0:
			return FVector4{0., 0., 1., 1.};
		case 1:
			return FVector4{0., 1., 0., 1.};
		case 2:
			return FVector4{1., 0., 0., 1.};
		case 3:
			return FVector4{1., 1., 0., 1.};
		case 4:
			return FVector4{1., 0., 1., 1.};
		case 5:
			return FVector4{0., 1., 1., 1.};
		case 6:
			return FVector4{ 1., .5, 0., 1. };
		case 7:
			return FVector4{ .5, 0., 1., 1. };
		case 8:
			return FVector4{ 0., 1., .5, 1. };
		case 9:
			return FVector4{ 0., .5, 1., 1. };
		case 10:
			return FVector4{ .5, 1., 0., 1. };
		case 11:
			return FVector4{ .2, .5, .5, 1. };
		default:
			const float val = StaticCast<float>(team_count_ - team - 1) / StaticCast<float>(team_count_ - 13);
			return FVector4{ val, val, val };
		}
}

int32 AGameplayGameMode::GetGameFieldHeight() const
{
	return game_field_height_;
}

int32 AGameplayGameMode::GetGameFieldWidth() const
{
	return game_field_width_;
}

double AGameplayGameMode::GetGameFieldHalfDistanceBetweenCells() const
{
	return game_field_half_distance_between_cells_;
}

ACameraPawn* AGameplayGameMode::GetCameraPawn() const {
	return camera_;
}

bool AGameplayGameMode::IsInbetweenTurns() const
{
	return inbetween_turns_;
}

void AGameplayGameMode::SetInbetweenTurns(const bool inbetween_turns)
{
	if (inbetween_turns_ && !inbetween_turns)
		Replay();
	inbetween_turns_ = inbetween_turns;
}

bool AGameplayGameMode::DisplayTutorial() const
{
	return display_tutorial_;
}

void AGameplayGameMode::SetDisplayTutorial(const bool display_tutorial)
{
	display_tutorial_ = display_tutorial;
}

bool AGameplayGameMode::DisplayGameOver() const
{
	return team_game_over_[current_team_] && !inbetween_turns_ && !replay_in_progress_ && !IsPaused();
}

bool AGameplayGameMode::DisplayVictory() const
{
	return !team_game_over_[current_team_] && remaining_team_count_ == 1 && !inbetween_turns_;
}

bool AGameplayGameMode::DisplayUI() const
{
	return !DisplayGameOver() && !DisplayVictory() && !display_tutorial_ && !inbetween_turns_ && !replay_in_progress_ && !IsPaused();
}

ASomething* AGameplayGameMode::SpawnFromWidget(USomethingFactoryWidget* widget)
{
	if (!Cast<ABasicFactory>(selected_something_) && !Cast<ABuilder>(selected_something_)) {
		return nullptr;
	}
	if (!widget->CanBeCreated()) {
		return nullptr;
	}
	resources_[current_team_] -= widget->GetResources();
	if (ABasicFactory* ptr = Cast<ABasicFactory>(selected_something_)) {
		return ptr->CreateUnit(widget);
	}
	if (ABuilder* ptr2 = Cast<ABuilder>(selected_something_)) {
		return ptr2->CreateBuilding(widget);
	}
	return nullptr;
}

void AGameplayGameMode::ClickedOnCell(ACell* cell)
{
	if (inbetween_turns_ || display_tutorial_ || replay_in_progress_) {
		return;
	}
	if (!selected_something_) { // nothing selected
		return;
	}
	AUnit* unit = Cast<AUnit>(selected_something_);
	if (!unit) { // building selected
		SetSelectedSomething(nullptr);
		return;
	}
	if (!unit->GetMobility() || !unit->CanMoveTo(cell)) { // can't move to the cell
		SetSelectedSomething(nullptr);
		return;
	}
	unit->MoveTo(cell); // move to the cell
	SetSelectedSomething(selected_something_); // update highlight effects
}

void AGameplayGameMode::ClickedOnSomething(ASomething* something)
{
	if (inbetween_turns_ || display_tutorial_ || replay_in_progress_) {
		return;
	}
	if (!something->GetHealth(current_turn_))
		return;
	if (selected_something_) {
		if (something == selected_something_) { // deselect
			SetSelectedSomething(nullptr);
			return;
		}
		AUnit* unit = Cast<AUnit>(selected_something_);
		if (!unit || !unit->GetAttackCount() || !unit->CanAttack(something)) { // can't attack
			if (current_team_ == something->GetTeam()) { // if same team, select new
				SetSelectedSomething(something);
			}
			else { // else, deselect
				SetSelectedSomething(nullptr);
			}
			return;
		}
		unit->Attack(something);
		SetSelectedSomething(selected_something_->GetHealth(current_turn_) ? selected_something_ : nullptr); // update highlight effects
		return;
	}
	if (current_team_ == something->GetTeam()) { // if same team, select
		SetSelectedSomething(something);
	}
	else { // else, deselect
		SetSelectedSomething(nullptr);
	}
}

void AGameplayGameMode::SetSelectedSomething(ASomething* something)
{
	if (selected_something_) {
		// remove previous selected effects
		for (ACell* cell : highlighted_cells_) {
			cell->Highlight(false);
		}
		highlighted_cells_.Reset();

		for (ASomething* smth : highlighted_somethings_) {
			smth->Highlight(HighlightType::NONE);
		}
		highlighted_somethings_.Reset();
	}
	selected_something_ = something;
	if (something) {
		// create selected effects
		if (const AUnit* ptr = Cast<AUnit>(something)) {
			highlighted_cells_ = ptr->GetMoveToCells();
			for (ACell* cell : highlighted_cells_) {
				cell->Highlight(true);
			}

			const HighlightType type = ptr->GET_ATTACK_DAMAGE() < 0 ? HighlightType::FRIEND : HighlightType::ENEMY;
			highlighted_somethings_ = ptr->GetAttackableSomethings();
			for (ASomething* smth : highlighted_somethings_) {
				smth->Highlight(type);
			}
		}

		something->Highlight(HighlightType::SELECTED);
		highlighted_somethings_.Add(something);
	}
}

void AGameplayGameMode::RequestNextTurn()
{
	if (!inbetween_turns_ && !display_tutorial_ && !animator_->AnimationInProgress() && !replay_in_progress_ && !IsPaused()) {
		GlobalNextTurn();
	}
}

void AGameplayGameMode::GlobalNextTurn()
{
	// start turn HUD
	SetInbetweenTurns(true);

	SetSelectedSomething(nullptr);
	camera_locations_[current_team_] = camera_->GetActorLocation();
	do {
		replay_stack_size_at_end_of_turn_.Add(replayer_->GetReplayStackSize());
		game_field_->PropogateNextTurn();
		++current_turn_;
		current_team_ = (current_team_ + 1) % team_count_;
	} while (team_viewed_game_over_[current_team_]); // skip lost players
	if (current_turn_ < 2u * team_count_) { // set commander selected on first turn, display tutorial
		SetSelectedSomething(GetSomethingsOfClassAndTeam(ACommander::StaticClass(), current_team_)[0]);
		SetDisplayTutorial(true);
	}
	if (team_game_over_[current_team_] && !team_viewed_game_over_[current_team_]) {
		team_viewed_game_over_[current_team_] = true; // show that you've lost once
		--remaining_team_count_;
	}
	camera_->SetActorLocation(camera_locations_[current_team_]);
	PreReplay();
}

void AGameplayGameMode::GameOverForTeam(const uint8 team)
{
	team_game_over_[team] = true;

	float delay = 0.f;
	for (ASomething* something : somethings_) {
		if (!something) {
			continue;
		}
		if (something->GetTeam() == team && something->GetHealth(current_turn_)) {
			ACell* cell = something->GetCell(current_turn_);
			something->Destroy(false);
			// for replay
			for (int32 i = 0; i < team_count_; ++i)
				replayer_->AddSomethingChange(something, true, i);

			if (cell->ViewerCounts(current_team_, current_turn_)) {
				// animation
				animator_->StopAnimation(something->GetLastAnimationId(), true);
				something->GetLastAnimationId() = animator_->AnimateDestroy(something, delay);
				delay += .1f;
			} else {
				replayer_->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), something, nullptr, 1.f);
			}
		}
	}
}

bool AGameplayGameMode::IsTeamGameOver(const uint8 team) const
{
	return team_game_over_[team];
}

void AGameplayGameMode::AddSomething(ASomething* something, const TSubclassOf<ASomething> subclass, const uint8 team)
{
	somethings_.Add(something);
	if (!class_team_somethings_.Contains(subclass)) {
		F2DASomethingTArrayStruct t;
		t._.Init({ {} }, team_count_);
		class_team_somethings_.Add(subclass, t);
	}
	class_team_somethings_[subclass]._[team]._.Add(something);
}

void AGameplayGameMode::UpdateCellMeshes(ACell* cell, const uint32 turn) const
{
	if (cell->ViewerCounts(current_team_, turn)) { // set all meshes on cell and cell visible
		cell->SetMaterialDesaturated(false);
		cell->SetCellHidden(false);
		if (ABuilding* ptr = cell->occupying_building_[turn]) {
			ptr->SetSomethingHidden(false);
		}
		if (AAerialUnit* ptr = cell->occupying_aerial_unit_[turn]) {
			ptr->SetSomethingHidden(false);
		}
		if (AGroundUnit* ptr = cell->occupying_ground_unit_[turn]) {
			ptr->SetSomethingHidden(false);
		}
		return;
	}
	// set all meshes invisible
	if (ABuilding* ptr = cell->occupying_building_[turn]) {
		ptr->SetSomethingHidden(true);
	}
	if (AAerialUnit* ptr = cell->occupying_aerial_unit_[turn]) {
		ptr->SetSomethingHidden(true);
	}
	if (AGroundUnit* ptr = cell->occupying_ground_unit_[turn]) {
		ptr->SetSomethingHidden(true);
	}

	if (cell->discovered_[current_team_]) { // set cell mesh desaturated but visible
		cell->SetMaterialDesaturated(true);
		cell->SetCellHidden(false);
	}
	else { // set cell mesh invisible
		cell->SetCellHidden(true);
	}
}

void AGameplayGameMode::UpdateAllMeshes(const uint32 turn) const
{
	for (ASomething* something : somethings_) {
		something->SetSomethingHidden(!something->GetHealth(turn));
		if (something->GetHealth(turn)) {
			ACell* cell = something->GetCell(turn);
			const double x = 50. * StaticCast<double>(cell->x_), y = 50. * StaticCast<double>(cell->y_) * UE_SQRT_3;
			if (Cast<ABuilding>(something)) {
				something->SetActorLocation(FVector(x, y - 20., 13.5 + 2. * (cell->type_ == CellType::GLACIER)));
			}
			else if (Cast<AAerialUnit>(something)) {
				something->SetActorLocation(FVector(x, y + 10., 60.));
			}
			else {
				something->SetActorLocation(FVector(x, y + 25. * !!cell->occupying_building_[turn],
					13.5 + 21.5 * (cell->type_ == CellType::SNOWY_MOUNTAINS || cell->type_ == CellType::MOUNTAINS) + 19.5 * (cell->type_ == CellType::RAISED_GLACIER) + 2. * (cell->type_ == CellType::GLACIER) + 2. * (cell->type_ == CellType::GLACIER && cell->occupying_building_[turn])));
			}
		}
	}
	for (ACell* cell : game_field_->GetAllCells()) {
		UpdateCellMeshes(cell, turn);
	}
}

void AGameplayGameMode::PreReplay() const
{
	if (current_turn_ < team_count_)
		return;
	UpdateAllMeshes(current_turn_ - team_count_);
}
void AGameplayGameMode::Replay()
{
	if (current_turn_ < team_count_)
		return;
	replayer_->ReplayFromIndex(replay_stack_size_at_end_of_turn_[current_turn_ - team_count_], current_team_);
	replay_in_progress_ = true;
}
void AGameplayGameMode::PostReplay()
{
	UpdateAllMeshes(current_turn_);
	// add resources
	for (ASomething* something : GetSomethingsOfClassAndTeam(AStation::StaticClass(), current_team_)) {
		if (something->GetHealth(current_turn_)) {
			resources_[current_team_] += Cast<AStation>(something)->GetResources();
		}
	}
	for (ASomething* something : GetSomethingsOfClassAndTeam(ARollback::StaticClass(), current_team_)) {
		if (something->GetHealth(current_turn_)) {
			Cast<ARollback>(something)->AccumulateCharge();
		}
	}
	for (ASomething* something : GetSomethingsOfClassAndTeam(AGroundhog::StaticClass(), current_team_)) {
		if (something->GetHealth(current_turn_)) {
			Cast<AGroundhog>(something)->ResetWarp();
		}
	}
	replay_in_progress_ = false;
}

void AGameplayGameMode::BeginPlay()
{
	Super::BeginPlay();

	// loading options
	if (const UMyGameInstance* game_instance = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this))) {
		team_count_ = game_instance->team_count_;
		game_field_height_ = game_instance->game_field_height_;
		game_field_width_ = game_instance->game_field_width_;
	}
	
	// generate game field
	game_field_ = GetWorld()->SpawnActor<AGameField>();
	game_field_->Generate(game_field_height_, game_field_width_, game_field_half_distance_between_cells_);

	// create animator and replayer
	animator_ = GetWorld()->SpawnActor<AAnimator>();
	replayer_ = GetWorld()->SpawnActor<AReplayer>();

	// init resources
	resources_.Init({0, 25, 0}, team_count_);
	//resources_.Init({9999, 9999, 9999}, team_count_);

	// get camera pawn
	camera_ = Cast<ACameraPawn>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPawn());
	camera_->UpdateCapValues(game_field_half_distance_between_cells_, game_field_height_, game_field_width_);
	
	// init game over
	team_game_over_.Init(false, team_count_);
	team_viewed_game_over_.Init(false, team_count_);
	remaining_team_count_ = team_count_;

	// brute force decently far away spawn points
	const int32 iterations = 20 * team_count_;
	TArray<ACell*> best_cells;
	int32 best_distance = 0;
	while (best_distance == 0) {
		for (int32 k = 0; k < iterations; ++k) {
			TArray<ACell*> cells;
			int32 min_distance = INT32_MAX;
			for (uint8 i = 0; i < team_count_; ++i) {
				ACell* rand_cell;
				do {
					const int32 idx = FMath::RandRange(0, game_field_height_ * game_field_width_ - 1);
					rand_cell = game_field_->GetAllCells()[idx];
				} while (rand_cell->type_ == CellType::WATER);
				min_distance = FGenericPlatformMath::Min(min_distance, FGenericPlatformMath::Min(rand_cell->y_, game_field_height_ - rand_cell->y_ - 1) * 2);
				for (ACell* cell : cells) {
					min_distance = FGenericPlatformMath::Min(min_distance, game_field_->GetDistance(cell, rand_cell));
				}
				cells.Add(rand_cell);
			}
			if (best_distance < min_distance) {
				best_distance = min_distance;
				Swap(best_cells, cells);
			}
		}
	}

	// spawn commanders
	for (uint8 i = 0; i < team_count_; ++i) {
		current_team_ = i;
		ACommander* spawned_commander = Cast<ACommander>(GetWorld()->SpawnActor(ACommander::StaticClass()));
		best_cells[i]->occupying_ground_unit_[0] = spawned_commander;
		spawned_commander->Init(best_cells[i], 0, i, false);
		camera_locations_.Add(spawned_commander->GetActorLocation() + FVector(0., 500., 500.));
		AddSomething(spawned_commander, ACommander::StaticClass(), i);
		spawned_commander->SET_STATS(TEXT("Health: 100, Power: 50, Mobility: 2, Tenacity: 1, Sight: 2, Range: 1"));
		spawned_commander->SET_DESCRIPTION(TEXT("This is the team's most important unit. Destruction of this unit means defeat for its team. Can create and destroy allied Buildings. Can use an attack to extract resources from the cell it's standing on."));
	}
	current_team_ = 0;

	// set camera location
	camera_->SetActorLocation(camera_locations_[0]);
	
	// padding
	for (uint8 i = 0; i < team_count_; ++i) {
		GlobalNextTurn();
	}
}

