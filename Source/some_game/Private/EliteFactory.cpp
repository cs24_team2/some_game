// cs24_team2 (c) BY


#include "EliteFactory.h"

// Sets default values
AEliteFactory::AEliteFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/BuildingMeshes/elite_factory.elite_factory'")));
}

int32 AEliteFactory::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 AEliteFactory::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

int32 AEliteFactory::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString AEliteFactory::GET_NAME() const
{
	return TEXT("Elite Factory");
}

// Called when the game starts or when spawned
void AEliteFactory::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AEliteFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

