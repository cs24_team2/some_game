// cs24_team2 (c) BY


#include "Scout.h"
#include "GameplayGameMode.h"
#include "GameField.h"

// Sets default values
AScout::AScout()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/scout.scout'")));
}

void AScout::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AScout::CanAttack(ASomething* target) const {
	return false;
}

int32 AScout::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 AScout::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AScout::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AScout::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AScout::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AScout::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AScout::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AScout::GET_ATTACK_DAMAGE() const{
	return ATTACK_DAMAGE;
}

int32 AScout::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AScout::GET_NAME() const
{
	return TEXT("Scout");
}

// Called when the game starts or when spawned
void AScout::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AScout::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

