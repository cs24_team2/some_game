// cs24_team2 (c) BY


#include "GroundFactory.h"

// Sets default values
AGroundFactory::AGroundFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/BuildingMeshes/ground_factory.ground_factory'")));
}

int32 AGroundFactory::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 AGroundFactory::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

int32 AGroundFactory::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString AGroundFactory::GET_NAME() const
{
	return TEXT("Ground Factory");
}

// Called when the game starts or when spawned
void AGroundFactory::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGroundFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

