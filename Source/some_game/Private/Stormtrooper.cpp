// cs24_team2 (c) BY


#include "Stormtrooper.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "GroundUnit.h"

// Sets default values
AStormtrooper::AStormtrooper()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/strike_aircraft.strike_aircraft'")));
}

void AStormtrooper::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AStormtrooper::CanAttack(ASomething* target) const {
	if (Cast<AAerialUnit>(target) != nullptr) {
		return false;
	}
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 AStormtrooper::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 AStormtrooper::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AStormtrooper::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AStormtrooper::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AStormtrooper::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AStormtrooper::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AStormtrooper::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AStormtrooper::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AStormtrooper::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AStormtrooper::GET_NAME() const
{
	return TEXT("Strike Aircraft");
}

// Called when the game starts or when spawned
void AStormtrooper::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStormtrooper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

