// cs24_team2 (c) BY


#include "Outcast.h"

#include "GameplayGameMode.h"
#include "GameField.h"
#include "BasicFactory.h"
#include "Cell.h"
#include "GroundUnit.h"

// Sets default values
AOutcast::AOutcast()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/SM_TempAerialUnit.SM_TempAerialUnit'")));
}


void AOutcast::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AOutcast::CanAttack(ASomething* target) const {
	return target == this;
}

int32 AOutcast::Attack(ASomething* target) {
	int32 result = AttackPrimitive(target);
	ACell* cell = target->GetCell(current_turn_);
	target->SetSomethingHidden(true);
	if (ABuilding* ptr_b = cell->occupying_building_.Last()) {
		ptr_b->SetSomethingHidden(true);
	}

	if (AGroundUnit* ptr_g = cell->occupying_ground_unit_.Last()) {
		ptr_g->SetSomethingHidden(true);
	}
	cell->SetCellHidden(true);
	return result;
}

void AOutcast::SetVisible() { // TO FIX. objects can reapper in the same move
	ACell* cell = GetCell(current_turn_);
	SetSomethingHidden(false);
	if (ABuilding* ptr_b = cell->occupying_building_.Last()) {
		ptr_b->SetSomethingHidden(false);
	}

	if (AGroundUnit* ptr_g = cell->occupying_ground_unit_.Last()) {
		ptr_g->SetSomethingHidden(false);
	}
	cell->SetCellHidden(false);
}

int32 AOutcast::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AOutcast::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AOutcast::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AOutcast::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AOutcast::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AOutcast::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AOutcast::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AOutcast::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AOutcast::GET_NAME() const
{
	return TEXT("Outcast");
}

// Called when the game starts or when spawned
void AOutcast::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOutcast::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

