// cs24_team2 (c) BY


#include "AerialUnit.h"

#include "Animator.h"
#include "Cell.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "Replayer.h"

// Sets default values
AAerialUnit::AAerialUnit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

TArray<ACell*> AAerialUnit::GetMoveToCells() const
{
	if (!GetMobility()) {
		return {};
	}
	ACell* cell = GetCell(current_turn_);
	if (!cell) {
		return {};
	}
	TArray<ACell*> neighbours = game_mode_->GetGameField()->GetNeighbours(cell);
	TArray<ACell*> answer;
	for (ACell* neighbour : neighbours) {
		if (CanMoveTo(neighbour)) {
			answer.Add(neighbour);
		}
	}

	return answer;
}

bool AAerialUnit::CanMoveTo(ACell* cell) const
{
	if (cell->occupying_aerial_unit_[current_turn_]) {
		return false;
	}

	return game_mode_->GetGameField()->AreNeighbours(GetCell(current_turn_), cell);;
}

void AAerialUnit::Destroy(const bool animate)
{
	GetCell(current_turn_)->occupying_aerial_unit_[current_turn_] = nullptr;
	ModifyVisibleCellViewerCounts(current_turn_, -1);
	destruction_turn_ = current_turn_;
	// game_mode_->UpdateSomethingVisibility(this, current_turn_);

	if (animate) {
		// for replay
		const int32 team_count = game_mode_->GetTeamCount();
		AReplayer* replayer = game_mode_->GetReplayer();
		for (int32 i = 0; i < team_count; ++i)
			replayer->AddSomethingChange(this, true, i);

		// animation
		game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
		last_animation_id_ = game_mode_->GetAnimator()->AnimateDestroy(this);
	}
}

// Called when the game starts or when spawned
void AAerialUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

void AAerialUnit::MoveToPrimitive(ACell* cell)
{
	--mobility_;
	const int32 prev_x = GetCell(current_turn_)->x_;
	GetCell(current_turn_)->occupying_aerial_unit_[current_turn_] = nullptr;
	cell->occupying_aerial_unit_[current_turn_] = this;
	SetCell(cell, current_turn_);

	// animation
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	const FVector from = GetActorLocation() + FVector(100.f * game_mode_->GetGameFieldWidth() * (FMath::Abs(cell->x_ - prev_x) > 3) * (1 - 2 * (cell->x_ < prev_x)), 0.f, 0.f);
	const FVector to = FVector(50. * StaticCast<double>(cell->x_),
		50. * StaticCast<double>(cell->y_) * UE_SQRT_3 + 10.,
		60.);
	last_animation_id_ = game_mode_->GetAnimator()->AnimateMove(this, from, to);
}

// Called every frame
void AAerialUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

