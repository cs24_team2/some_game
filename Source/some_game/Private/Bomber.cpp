// cs24_team2 (c) BY


#include "Bomber.h"

#include "Building.h"
#include "Cell.h"
#include "GroundUnit.h"

// Sets default values
ABomber::ABomber()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/bomber.bomber'")));
}

int32 ABomber::Attack(ASomething* target)
{
	int32 out = 0;
	ACell* cell = target->GetCell(current_turn_);
	if (AGroundUnit* ptr = cell->occupying_ground_unit_[current_turn_]) {
		out += AttackPrimitive(ptr);
	}
	if (ABuilding* ptr = cell->occupying_building_[current_turn_]) {
		out += AttackPrimitive(ptr);
	}
	attack_count_ = 0;
	return out;
}

int32 ABomber::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 ABomber::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 ABomber::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 ABomber::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

int32 ABomber::GET_VISIBILITY_DISTANCE() const
{
	return VISIBLE_DISTANCE;
}

int32 ABomber::GET_ATTACK_DISTANCE() const
{
	return ATTACK_DISTANCE;
}

FString ABomber::GET_NAME() const
{
	return TEXT("Bomber");
}

// Called when the game starts or when spawned
void ABomber::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABomber::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

