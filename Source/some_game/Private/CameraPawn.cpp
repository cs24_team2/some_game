// cs24_team2 (c) BY


#include "CameraPawn.h"

#include <Kismet/GameplayStatics.h>
#include "Camera/CameraComponent.h"
#include "Blueprint/UserWidget.h"

#include "GameplayGameMode.h"
#include "CameraPlayerController.h"

// Sets default values
ACameraPawn::ACameraPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	camera_ = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));
	camera_->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACameraPawn::BeginPlay()
{
	Super::BeginPlay();
	
	camera_->SetWorldRotation(FRotator(-45., -90., 0.));
	camera_->SetFieldOfView(42.);

	game_mode_ = (AGameplayGameMode*)(UGameplayStatics::GetGameMode(this));

	y_cap_ = game_mode_->GetGameFieldHalfDistanceBetweenCells() * UE_SQRT_3 * (game_mode_->GetGameFieldHeight() - 1);
	x_cap_ = game_mode_->GetGameFieldHalfDistanceBetweenCells() * 2. * game_mode_->GetGameFieldWidth();
	upper_bound_ = FGenericPlatformMath::Min(140. * game_mode_->GetGameFieldWidth(), 1400.);
}

// Called every frame
void ACameraPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (game_mode_->IsInbetweenTurns() || game_mode_->DisplayTutorial()) {
		return;
	}

	FVector new_location = GetActorLocation() + DeltaTime * (move_speed_ * move_vector_ + zoom_speed_ * zoom_vector_);
	if (new_location.Z > upper_bound_) {
		new_location += zoom_vector_ * (upper_bound_ - new_location.Z) / zoom_vector_.Z;
	}
	else if (new_location.Z < lower_bound_) {
		new_location += zoom_vector_ * (lower_bound_ - new_location.Z) / zoom_vector_.Z;
	}
	new_location.Y = FMath::Clamp(new_location.Y, new_location.Z, new_location.Z + y_cap_);
	if (new_location.X < 0) {
		new_location.X += x_cap_;
	}
	else if (new_location.X > x_cap_) {
		new_location.X -= x_cap_;
	}
	SetActorLocation(new_location);
}

// Called to bind functionality to input
void ACameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("AxisFB"), this, &ACameraPawn::MoveFB);
	PlayerInputComponent->BindAxis(TEXT("AxisLR"), this, &ACameraPawn::MoveLR);
	PlayerInputComponent->BindAxis(TEXT("AxisZoom"), this, &ACameraPawn::Zoom);
	PlayerInputComponent->BindAction(TEXT("SpaceBar"), IE_Pressed, this, &ACameraPawn::OnSpaceBar);
	PlayerInputComponent->BindAction(TEXT("Escape"), IE_Pressed, this, &ACameraPawn::OnEscape).bExecuteWhenPaused = true;
}

void ACameraPawn::UpdateCapValues(const double half_distance_between_cells, const int32 height, const int32 width)
{
	y_cap_ = half_distance_between_cells * UE_SQRT_3 * (height - 1);
	x_cap_ = half_distance_between_cells * 2. * width;
	upper_bound_ = FGenericPlatformMath::Min(140. * width, 1400.);
}

void ACameraPawn::MoveFB(float amount)
{
	move_vector_.Y = -amount;
}

void ACameraPawn::MoveLR(float amount)
{
	move_vector_.X = -amount;
}

void ACameraPawn::Zoom(float amount)
{
	FVector dummy, world_direction;
	GetLocalViewingPlayerController()->DeprojectMousePositionToWorld(dummy, world_direction);
	zoom_vector_ = world_direction * amount;
}

void ACameraPawn::OnSpaceBar()
{
	if (game_mode_->IsInbetweenTurns()) {
		game_mode_->SetInbetweenTurns(false);
	}
	else {
		game_mode_->RequestNextTurn();
	}
}

void ACameraPawn::OnEscape()
{
	if (game_mode_->IsPaused()) {
		game_mode_->ClearPause();
	} else {
		game_mode_->SetPause(GetLocalViewingPlayerController());
	}
}

