// cs24_team2 (c) BY


#include "BasicFactory.h"

#include "Cell.h"
#include "SomethingFactoryWidget.h"
#include "Unit.h"
#include "AerialUnit.h"
#include "GroundUnit.h"

// Sets default values
ABasicFactory::ABasicFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

AUnit* ABasicFactory::CreateUnit(USomethingFactoryWidget* factory)
{
	AUnit* created_unit = Cast<AUnit>(factory->Create(team_));
	if (Cast<AAerialUnit>(created_unit)) {
		underlying_cell_.Last()->occupying_aerial_unit_.Last() = Cast<AAerialUnit>(created_unit);
	}
	else {
		underlying_cell_.Last()->occupying_ground_unit_.Last() = Cast<AGroundUnit>(created_unit);
	}
	created_unit->Init(underlying_cell_.Last(), current_turn_, team_);
	created_units_.Add(created_unit);
	return created_unit;
}

TArray<AUnit*>& ABasicFactory::GetCreatedUnits() {
	return created_units_;
}


// Called when the game starts or when spawned
void ABasicFactory::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

