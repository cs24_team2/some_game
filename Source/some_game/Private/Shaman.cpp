// cs24_team2 (c) BY


#include "Shaman.h"

#include "Animator.h"
#include "GameField.h"
#include "GameplayGameMode.h"

// Sets default values
AShaman::AShaman()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/shaman.shaman'")));
}

bool AShaman::CanAttack(ASomething* target) const
{
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	if (target->GetHealth(current_turn_) >= target->GET_MAX_HEALTH()) {
		return false;
	}

	return team_ == target->GetTeam();
}

int32 AShaman::Attack(ASomething* target)
{
	--attack_count_;
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	last_animation_id_ = game_mode_->GetAnimator()->AnimateHeal(this, target, 50);
	return target->Heal(target->GET_MAX_HEALTH() - target->GetHealth(current_turn_));
}

int32 AShaman::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 AShaman::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 AShaman::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 AShaman::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

int32 AShaman::GET_VISIBILITY_DISTANCE() const
{
	return VISIBLE_DISTANCE;
}

int32 AShaman::GET_ATTACK_DISTANCE() const
{
	return ATTACK_DISTANCE;
}

FString AShaman::GET_NAME() const
{
	return TEXT("Shaman");
}

// Called when the game starts or when spawned
void AShaman::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShaman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

