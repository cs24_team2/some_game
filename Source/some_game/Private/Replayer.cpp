// cs24_team2 (c) BY


#include "Replayer.h"

#include "Cell.h"
#include "Commander.h"
#include "GameplayGameMode.h"
#include "Something.h"
#include "Kismet/GameplayStatics.h"

void AReplayer::AddAnimationToReplay(const EAnimationType type, const FVector& from, const FVector& to, ASomething* a, ASomething* b, const float duration, const int32 value)
{
	++current_index_;
	replay_animations_stack_.Emplace(type, from, to, a, b, duration, value);
	Swap(replay_animations_stack_.Last().cell_changes_, current_cell_changes_);
	Swap(replay_animations_stack_.Last().something_changes_, current_something_changes_);
}

void AReplayer::AddCellChange(ACell* cell, bool desaturated, uint8 team)
{
	current_cell_changes_.Emplace(cell, desaturated, team);
}

void AReplayer::AddSomethingChange(ASomething* something, bool hidden, uint8 team)
{
	current_something_changes_.Emplace(something, hidden, team);
}

void AReplayer::ReplayFromIndex(const int32 index, const uint8 team)
{
	current_index_ = index;
	team_ = team;
	replaying_ = true;
	replayed_commander_destruction_for_team_.Init(false, game_mode_->GetTeamCount());
}

uint32 AReplayer::GetReplayStackSize() const
{
	return replay_animations_stack_.Num();
}

bool AReplayer::Replaying() const
{
	return replaying_;
}

// Sets default values
AReplayer::AReplayer()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AReplayer::BeginPlay()
{
	Super::BeginPlay();

	game_mode_ = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));

	replayed_commander_destruction_for_team_.Init(false, game_mode_->GetTeamCount());
}

// Called every frame
void AReplayer::Tick(const float delta_time)
{
	Super::Tick(delta_time);

	if (!replaying_)
		return;

	if (wait_ > 0.f) {
		wait_ -= delta_time;
		return;
	}

	while (wait_ <= 0.f) {
		for (const FCellChange& change : end_cell_changes_) {
			change.cell_->SetMaterialDesaturated(change.desaturated_);
			change.cell_->SetCellHidden(false);
		}
		end_cell_changes_.Empty();

		for (const FSomethingChange& change : end_something_changes_)
			change.something_->SetSomethingHidden(change.hidden_);
		end_something_changes_.Empty();

		if (current_index_ >= replay_animations_stack_.Num()) {
			replaying_ = false;
			game_mode_->PostReplay();
			return;
		}

		auto& [type, from, to, a, b, duration, value, cell_changes, something_changes] = replay_animations_stack_[current_index_];
		const bool reverse_changes_order = type == EAnimationType::CREATE;
		for (const FCellChange& change : cell_changes)
			if (team_ == change.team_)
				if (change.desaturated_ != reverse_changes_order) {
					end_cell_changes_.Add(change);
				}
				else {
					change.cell_->SetMaterialDesaturated(change.desaturated_);
					change.cell_->SetCellHidden(false);
				}
		for (const FSomethingChange& change : something_changes)
			if (team_ == change.team_)
				if (change.hidden_ != reverse_changes_order) {
					end_something_changes_.Add(change);
				}
				else {
					change.something_->SetSomethingHidden(change.hidden_);
				}
		bool show_animation = true;
		if (!(a && !a->IsSomethingHidden()) && !(b && !b->IsSomethingHidden())) {
			show_animation = false;
			for (const FSomethingChange& change : end_something_changes_) {
				if ((change.something_ == a || change.something_ == b) && !change.hidden_ && team_ == change.team_) {
					show_animation = true;
					break;
				}
			}
		}
		if (show_animation) {
			game_mode_->GetAnimator()->AddRawAnimation(type, from, to, a, b, duration, value);
			wait_ = duration;
			if (type == EAnimationType::DESTROY && a && replayed_commander_destruction_for_team_[a->GetTeam()])
				wait_ = .1f;
		} else {
			// animation moves something
			if (type == EAnimationType::MOVE || type == EAnimationType::TELEPORT) {
				const uint32 id = game_mode_->GetAnimator()->AddRawAnimation(type, from, to, a, b, duration, value);
				game_mode_->GetAnimator()->StopAnimation(id, true);
			}
		}
		if (type == EAnimationType::DESTROY && Cast<ACommander>(a)) {
			replayed_commander_destruction_for_team_[a->GetTeam()] = true;
		}
		++current_index_;
		if (current_index_ >= replay_animations_stack_.Num() && replayed_commander_destruction_for_team_[team_])
			wait_ = 1.f;
	}
}
