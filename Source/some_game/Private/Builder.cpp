// cs24_team2 (c) BY


#include "Builder.h"

#include "Animator.h"
#include "Cell.h"
#include "SomethingFactoryWidget.h"
#include "Building.h"
#include "GameplayGameMode.h"

// Sets default values
ABuilder::ABuilder()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

bool ABuilder::CanGetResources() const
{
	return attack_count_ > 0;
}

void ABuilder::GetResources()
{
	if (CanGetResources()) {
		--attack_count_;
		FResources& team_resources = game_mode_->GetTeamResources(team_);
		switch (GetCell(current_turn_)->type_) {
		case CellType::GLACIER:
			team_resources += {3, 0, 0};
			return;
		case CellType::RAISED_GLACIER:
			team_resources += {3, 0, 0};
			return;
		case CellType::SNOWY_PLAINS:
			team_resources += {1, 2, 0};
			return;
		case CellType::SNOWY_MOUNTAINS:
			team_resources += {1, 2, 0};
			return;
		case CellType::PLAINS:
			team_resources += {0, 3, 0};
			return;
		case CellType::MOUNTAINS:
			team_resources += {0, 3, 0};
			return;
		case CellType::WASTELAND:
			team_resources += {0, 2, 1};
			return;
		case CellType::DESERT:
			team_resources += {0, 0, 3};
			return;
		default:
			return;
		}
	}
}

ABuilding* ABuilder::CreateBuilding(USomethingFactoryWidget* factory) {
	ACell* cell = underlying_cell_.Last();
	game_mode_->GetAnimator()->AnimateMove(this, GetActorLocation(),FVector(50. * StaticCast<double>(cell->x_), 50. * StaticCast<double>(cell->y_) * UE_SQRT_3 + 25.f,13.5f + 4.f * (cell->type_ == CellType::GLACIER)));
	ABuilding* created_building = Cast<ABuilding>(factory->Create(team_));
	underlying_cell_.Last()->occupying_building_.Last() = created_building;
	created_building->Init(underlying_cell_.Last(), current_turn_, team_);
	game_mode_->SetSelectedSomething(this);
	return created_building;
}

bool ABuilder::CanDestroyBuilding() const
{
	return underlying_cell_.Last()->occupying_building_.Last() && underlying_cell_.Last()->occupying_building_.Last()->GetTeam() == team_;
}

void ABuilder::DestroyBuilding() {
	if (CanDestroyBuilding()) {
		underlying_cell_.Last()->occupying_building_.Last()->Destroy();
	}
}


// Called when the game starts or when spawned
void ABuilder::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABuilder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

