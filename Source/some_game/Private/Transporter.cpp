// cs24_team2 (c) BY


#include "Transporter.h"

// Sets default values
ATransporter::ATransporter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/BuildingMeshes/transporter.transporter'")));
}

int32 ATransporter::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 ATransporter::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

int32 ATransporter::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString ATransporter::GET_NAME() const
{
	return TEXT("Transporter");
}

// Called when the game starts or when spawned
void ATransporter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATransporter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

