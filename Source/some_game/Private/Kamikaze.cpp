// cs24_team2 (c) BY

#include "Kamikaze.h"

#include "GameplayGameMode.h"
#include "GameField.h"
#include "AerialUnit.h"
#include "Animator.h"
#include "Building.h"
#include "Cell.h"
#include "GroundUnit.h"
#include "Replayer.h"

// Sets default values
AKamikaze::AKamikaze()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/kamikaze.kamikaze'")));
}

void AKamikaze::MoveTo(ACell* cell)
{
	MoveToPrimitive(cell);
}

bool AKamikaze::CanAttack(ASomething* target) const
{
	if (Cast<AAerialUnit>(target) != nullptr) {
		return false;
	}

	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 AKamikaze::Attack(ASomething* target)
{
	int32 out = 0;
	ACell* cell = target->GetCell(current_turn_);
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	game_mode_->GetAnimator()->AnimateMove(this, GetActorLocation(), target->GetActorLocation());

	const int32 damage = GET_ATTACK_DAMAGE();
	const int32 team_count = game_mode_->GetTeamCount();
	AReplayer* replayer = game_mode_->GetReplayer();
	AAnimator* animator = game_mode_->GetAnimator();

	if (AGroundUnit* ptr = cell->occupying_ground_unit_[current_turn_]) {
		if (ptr->GetHealth(current_turn_) > damage) {
			out += ptr->GetHealth(current_turn_) - ptr->Damage(damage);
		} else {
			out += ptr->GetHealth(current_turn_);
			ptr->Destroy(false);
			// for destroy animation and replay
			for (int32 i = 0; i < team_count; ++i)
				replayer->AddSomethingChange(ptr, true, i);

			if (cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
				animator->StopAnimation(ptr->GetLastAnimationId(), true);
				ptr->GetLastAnimationId() = animator->AnimateDestroy(ptr, .5f);
			}
			else {
				replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), ptr, nullptr, 1.f);
			}
		}
	}
	if (ABuilding* ptr = cell->occupying_building_[current_turn_]) {
		if (ptr->GetHealth(current_turn_) > damage) {
			out += ptr->GetHealth(current_turn_) - ptr->Damage(damage);
		}
		else {
			out += ptr->GetHealth(current_turn_);
			ptr->Destroy(false);
			// for destroy animation and replay
			for (int32 i = 0; i < team_count; ++i)
				replayer->AddSomethingChange(ptr, true, i);

			if (cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
				animator->StopAnimation(ptr->GetLastAnimationId(), true);
				ptr->GetLastAnimationId() = animator->AnimateDestroy(ptr, .5f);
			}
			else {
				replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), ptr, nullptr, 1.f);
			}
		}
	}
	Destroy(false);
	for (int32 i = 0; i < team_count; ++i)
		replayer->AddSomethingChange(this, true, i);
	animator->StopAnimation(last_animation_id_, true);
	last_animation_id_ = animator->AnimateDestroy(this, .5f);
	return out;
}

int32 AKamikaze::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 AKamikaze::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

bool AKamikaze::IsVisible(ACell* cell, uint32 turn) const
{
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AKamikaze::GetVisibleCells(uint32 turn) const
{
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AKamikaze::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 AKamikaze::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 AKamikaze::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 AKamikaze::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString AKamikaze::GET_NAME() const
{
	return TEXT("Kamikaze");
}

// Called when the game starts or when spawned
void AKamikaze::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKamikaze::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

