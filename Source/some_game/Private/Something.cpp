// cs24_team2 (c) BY


#include "Something.h"

#include <Kismet/GameplayStatics.h>

#include "AerialUnit.h"
#include "Building.h"
#include "Cell.h"
#include "GameField.h"
#include "GameplayGameMode.h"
#include "Replayer.h"

// Sets default values
ASomething::ASomething()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void ASomething::Init(ACell* cell, uint32 creation_turn, uint8 team, bool animate)
{
	health_.Add(GET_MAX_HEALTH());
	underlying_cell_.Add(cell);
	creation_turn_ = creation_turn;
	current_turn_ = creation_turn;
	team_ = team;
	game_mode_ = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));
	const double shift = 2. * game_mode_->GetGameFieldHalfDistanceBetweenCells() * game_mode_->GetGameFieldWidth();
	scene_components_[1]->AddWorldOffset(FVector(-shift, 0., 0.));
	scene_components_[2]->AddWorldOffset(FVector(shift, 0., 0.));
	dynamic_highlight_material_ = UMaterialInstanceDynamic::Create(game_mode_->GetSomethingHighlightMaterial(), this);
	dynamic_team_material_ = UMaterialInstanceDynamic::Create(meshes_[0]->GetMaterial(0), this);
	for (UStaticMeshComponent* mesh : meshes_) {
		mesh->SetOverlayMaterial(dynamic_highlight_material_);
		mesh->SetMaterial(0, dynamic_team_material_);
	}
	dynamic_team_material_->SetVectorParameterValue("Team Color", game_mode_->GetTeamColor(team));
	ModifyVisibleCellViewerCounts(creation_turn, 1);
	const double x = 50. * StaticCast<double>(cell->x_), y = 50. * StaticCast<double>(cell->y_) * UE_SQRT_3;
	if (Cast<ABuilding>(this)) {
		SetActorLocation(FVector(x, y - 20., 13.5 + 2. * (cell->type_ == CellType::GLACIER)));
	} else if (Cast<AAerialUnit>(this)) {
		SetActorLocation(FVector(x, y + 10., 60.));
	} else {
		SetActorLocation(FVector(x, y + 25. * !!cell->occupying_building_[current_turn_], 
			13.5 + 21.5 * (cell->type_ == CellType::SNOWY_MOUNTAINS || cell->type_ == CellType::MOUNTAINS) + 19.5 * (cell->type_ == CellType::RAISED_GLACIER) + 2. * (cell->type_ == CellType::GLACIER) + 2. * (cell->type_ == CellType::GLACIER && cell->occupying_building_[current_turn_])));
	}

	if (animate) {
		// for replay
		const int32 team_count = game_mode_->GetTeamCount();
		AReplayer* replayer = game_mode_->GetReplayer();
		for (int32 i = 0; i < team_count; ++i)
			replayer->AddSomethingChange(this, !cell->ViewerCounts(i, creation_turn), i);

		// animation
		SetSomethingHidden(true);
		last_animation_id_ = game_mode_->GetAnimator()->AnimateCreate(this, GetActorLocation());
	}
}

void ASomething::NextTurn()
{
	if (current_turn_ < destruction_turn_) {
		ACell* cell_t = underlying_cell_.Last();
		underlying_cell_.Add(cell_t);
		int32 health_t = health_.Last();
		health_.Add(health_t);
		++current_turn_;
	}
}

ACell* ASomething::GetCell(uint32 turn) const
{
	if (creation_turn_ > turn || turn >= destruction_turn_) {
		return nullptr;
	}
	return underlying_cell_[turn - creation_turn_];
}

bool ASomething::SetCell(ACell* cell, uint32 turn)
{
	if (creation_turn_ > turn || turn >= destruction_turn_) {
		return false;
	}
	ModifyVisibleCellViewerCounts(turn, -1);
	underlying_cell_[turn - creation_turn_] = cell;
	ModifyVisibleCellViewerCounts(turn, 1);

	// for replay
	const int32 team_count = game_mode_->GetTeamCount();
	AReplayer* replayer = game_mode_->GetReplayer();
	for (int32 i = 0; i < team_count; ++i)
		replayer->AddSomethingChange(this, !cell->ViewerCounts(i, turn), i);
	return true;
}

int32 ASomething::GetHealth(uint32 turn) const
{
	if (creation_turn_ > turn || turn >= destruction_turn_) {
		return 0;
	}
	return health_[turn - creation_turn_];
}

int32 ASomething::GetCurrentHealth() const
{
	return GetHealth(current_turn_);
}

uint8 ASomething::GetTeam() const
{
	return team_;
}

void ASomething::ModifyVisibleCellViewerCounts(uint32 turn, int32 value)
{
	TArray<ACell*> cells = GetVisibleCells(turn);
	for (ACell* cell : cells) {
		cell->ModifyViewerCounts(team_, turn, value);
	}
}

void ASomething::SetSomethingHidden(bool hidden)
{
	for (UStaticMeshComponent* mesh : meshes_) {
		mesh->SetHiddenInGame(hidden);
		mesh->SetCollisionEnabled(hidden ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryOnly);
	}
	hidden_ = hidden;
}

bool ASomething::IsSomethingHidden() const
{
	return hidden_;
}

void ASomething::Highlight(HighlightType type)
{
	switch (type)
	{
		case HighlightType::NONE:
			dynamic_highlight_material_->SetVectorParameterValue("Line Color", FVector4{ 0. ,0. ,0. ,0. });
			return;
		case HighlightType::ENEMY:
			dynamic_highlight_material_->SetVectorParameterValue("Line Color", FVector4{ 1. ,0. ,0. ,.5 });
			return;
		case HighlightType::FRIEND:
			dynamic_highlight_material_->SetVectorParameterValue("Line Color", FVector4{ 0. ,1. ,0. ,.5 });
			return;
		case HighlightType::SELECTED:
			dynamic_highlight_material_->SetVectorParameterValue("Line Color", FVector4{ 0. ,1. ,1. ,.5 });
			return;
	}
}

const TArray<UStaticMeshComponent*>& ASomething::GetMeshes() const
{
	return meshes_;
}

uint32& ASomething::GetLastAnimationId()
{
	return last_animation_id_;
}

void ASomething::NotifyActorOnClicked(FKey ButtonPressed)
{
	if (!hidden_ && ButtonPressed == EKeys::LeftMouseButton) {
		game_mode_->ClickedOnSomething(this);
	}
}

bool ASomething::IsVisible(ACell* cell, uint32 turn) const
{
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= GET_VISIBILITY_DISTANCE();
}

TArray<ACell*> ASomething::GetVisibleCells(uint32 turn) const
{
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), GET_VISIBILITY_DISTANCE());
	result.Add(GetCell(turn));
	return result;
}

int32 ASomething::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 ASomething::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

void ASomething::SET_STATS(FString stats)
{
	stats_ = stats;
}

FString ASomething::GET_STATS() const
{
	return stats_;
}

void ASomething::SET_DESCRIPTION(FString desc)
{
	desc_ = desc;
}

FString ASomething::GET_DESCRIPTION() const
{
	return desc_;
}
// Called when the game starts or when spawned
void ASomething::BeginPlay()
{
	Super::BeginPlay();

	game_mode_ = (AGameplayGameMode*)(UGameplayStatics::GetGameMode(this));
}

void ASomething::SetMesh(const ConstructorHelpers::FObjectFinder<UStaticMesh>& mesh)
{
	USceneComponent* scene_component = CreateDefaultSubobject<USceneComponent>(TEXT("scene_component"));
	scene_component->SetupAttachment(GetRootComponent());
	scene_components_.Add(scene_component);
	UStaticMeshComponent* _mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	_mesh->SetupAttachment(scene_component);

	_mesh->SetMobility(EComponentMobility::Movable);
	if (mesh.Succeeded()) {
		_mesh->SetStaticMesh(mesh.Object);
	}

	meshes_.Add(_mesh);

	USceneComponent* left_scene_component = CreateDefaultSubobject<USceneComponent>(TEXT("left_scene_component"));
	left_scene_component->SetupAttachment(scene_component);
	scene_components_.Add(left_scene_component);
	UStaticMeshComponent* left_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("left_mesh"));
	left_mesh->SetupAttachment(left_scene_component);

	left_mesh->SetMobility(EComponentMobility::Movable);
	if (mesh.Succeeded()) {
		left_mesh->SetStaticMesh(mesh.Object);
	}

	meshes_.Add(left_mesh);

	USceneComponent* right_scene_component = CreateDefaultSubobject<USceneComponent>(TEXT("right_scene_component"));
	right_scene_component->SetupAttachment(scene_component);
	scene_components_.Add(right_scene_component);
	UStaticMeshComponent* right_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("right_mesh"));
	right_mesh->SetupAttachment(right_scene_component);

	right_mesh->SetMobility(EComponentMobility::Movable);
	if (mesh.Succeeded()) {
		right_mesh->SetStaticMesh(mesh.Object);
	}

	meshes_.Add(right_mesh);
}

void ASomething::SetMaterial(const ConstructorHelpers::FObjectFinder<UMaterial>& material)
{
	if (meshes_[0] && material.Succeeded()) {
		for (UStaticMeshComponent* mesh : meshes_) {
			mesh->SetMaterial(0, material.Object);
		}
	}
}

int32 ASomething::DamagePrimitive(int32 damage)
{
	if (damage >= health_[current_turn_ - creation_turn_]) {
		health_[current_turn_ - creation_turn_] = 0;
		Destroy();
	}
	else {
		health_[current_turn_ - creation_turn_] -= damage;
	}
	return health_[current_turn_ - creation_turn_];
}

int32 ASomething::HealPrimitive(int32 heal_amount)
{
	if (health_[current_turn_ - creation_turn_] + heal_amount > GET_MAX_HEALTH()) {
		health_[current_turn_ - creation_turn_] = GET_MAX_HEALTH();
	}
	else {
		health_[current_turn_ - creation_turn_] += heal_amount;
	}
	return health_[current_turn_ - creation_turn_];
}

// Called every frame
void ASomething::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

