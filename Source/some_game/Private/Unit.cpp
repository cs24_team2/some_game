// cs24_team2 (c) BY


#include "Unit.h"

#include "Cell.h"
#include "Building.h"
#include "GroundUnit.h"
#include "AerialUnit.h"
#include "Animator.h"
#include "GameField.h"
#include "GameplayGameMode.h"

// Sets default values
AUnit::AUnit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

int32 AUnit::GetMobility() const
{
	return mobility_;
}

void AUnit::ResetMobility()
{
	mobility_ = GET_MAX_MOBILITY();
}

int32 AUnit::GetAttackCount() const
{
	return attack_count_;
}

void AUnit::ResetAttackCount()
{
	attack_count_ = GET_MAX_ATTACK_COUNT();
}

TArray<ASomething*> AUnit::GetAttackableSomethings() const
{
	if (!GetAttackCount()) {
		return {};
	}
	TArray<ACell*> visible_cells = GetVisibleCells(current_turn_);
	TArray<ASomething*> answer;
	for (ACell* cell : visible_cells) {
		if (ASomething* ptr = cell->occupying_building_[current_turn_]) {
			if (CanAttack(ptr)) {
				answer.Add(ptr);
			}
		}
		if (ASomething* ptr = cell->occupying_aerial_unit_[current_turn_]) {
			if (CanAttack(ptr)) {
				answer.Add(ptr);
			}
		}
		if (ASomething* ptr = cell->occupying_ground_unit_[current_turn_]) {
			if (CanAttack(ptr)) {
				answer.Add(ptr);
			}
		}
	}

	return answer;
}

void AUnit::MoveTo(ACell* cell)
{
	MoveToPrimitive(cell);
}

bool AUnit::CanAttack(ASomething* target) const
{
	return CanAttackPrimitive(target);
}

int32 AUnit::Attack(ASomething* target)
{
	return AttackPrimitive(target);
}

// Called when the game starts or when spawned
void AUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

int32 AUnit::AttackPrimitive(ASomething* target) 
{
	--attack_count_;
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	last_animation_id_ = game_mode_->GetAnimator()->AnimateShoot(this, target, GET_ATTACK_DAMAGE());
	return target->GetHealth(current_turn_) - target->Damage(GET_ATTACK_DAMAGE());
}

bool AUnit::CanAttackPrimitive(const ASomething* target) const
{
	const AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > GET_ATTACK_DISTANCE()) {
		return false;
	}
	return team_ != target->GetTeam();
}

// Called every frame
void AUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

