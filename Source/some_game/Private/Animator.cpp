// cs24_team2 (c) BY


#include "Animator.h"

#include "GameplayGameMode.h"
#include "Replayer.h"
#include "Something.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Sets default values
AAnimator::AAnimator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	explosion_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Explosion/ExplosionEffectSystem.ExplosionEffectSystem'")).Object;
	creation_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Creation/CreationEffectSystem.CreationEffectSystem'")).Object;
	teleport_disappear_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Teleport/TeleportDisappearEffectSystem.TeleportDisappearEffectSystem'")).Object;
	teleport_appear_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Teleport/TeleportAppearEffectSystem.TeleportAppearEffectSystem'")).Object;
	shoot_laser_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Shoot/ShootEffectSystem.ShootEffectSystem'")).Object;
	heal_laser_niagara_ = ConstructorHelpers::FObjectFinder<UNiagaraSystem>(TEXT("/Script/Niagara.NiagaraSystem'/Game/Effects/Heal/HealEffectSystem.HealEffectSystem'")).Object;
}

uint32 AAnimator::AnimateMove(ASomething* something, const FVector& from, const FVector& to)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::MOVE, from, to, something, nullptr, time_, time_ + .5f });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::MOVE, from, to, something, nullptr, .5f);
	return next_id_++;
}

uint32 AAnimator::AnimateTeleport(ASomething* something, const FVector& from, const FVector& to)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::TELEPORT, from, to, something, nullptr, time_, time_ + 1.f });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::TELEPORT, from, to, something, nullptr, 1.f);
	return next_id_++;
}

uint32 AAnimator::AnimateCreate(ASomething* something, const FVector& to)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::CREATE, FVector(0.f), to, something, nullptr, time_, time_ + 1.5f });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::CREATE, FVector(0.f), to, something, nullptr, 1.5f);
	return next_id_++;
}

uint32 AAnimator::AnimateDestroy(ASomething* something, const float delay)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::DESTROY, FVector(0.f), FVector(0.f), something, nullptr, time_ + delay, time_ + delay + 1.f });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), something, nullptr, 1.f);
	return next_id_++;
}

uint32 AAnimator::AnimateShoot(ASomething* attacker, ASomething* target, const int32 damage)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::SHOOT, FVector(0.f), FVector(0.f), attacker, target, time_, time_ + .5f, damage });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::SHOOT, FVector(0.f), FVector(0.f), attacker, target, .5f, damage);
	return next_id_++;
}

uint32 AAnimator::AnimateHeal(ASomething* healer, ASomething* target, const int32 heal_amount)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::HEAL, FVector(0.f), FVector(0.f), healer, target, time_, time_ + .5f, heal_amount });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::HEAL, FVector(0.f), FVector(0.f), healer, target, .5f, heal_amount);
	return next_id_++;
}

uint32 AAnimator::AnimateDisappear(ASomething* something)
{
	animations_.Add(next_id_, FAnimation{ EAnimationType::DISAPPEAR, FVector(0.f), FVector(0.f), something, nullptr, time_, time_ + 1.f });
	game_mode_->GetReplayer()->AddAnimationToReplay(EAnimationType::DISAPPEAR, FVector(0.f), FVector(0.f), something, nullptr, 1.f);
	return next_id_++;
}

uint32 AAnimator::AddRawAnimation(const EAnimationType type, const FVector& from, const FVector& to, ASomething* a, ASomething* b, const float duration, const int32 value)
{
	animations_.Add(next_id_, FAnimation{ type, from, to, a, b, time_, time_ + duration, value});
	return next_id_++;
}

void AAnimator::StopAnimation(const uint32 id, const bool end_position)
{
	if (FAnimation* anim = animations_.Find(id)) {
		if (anim->done_)
			return;
		anim->done_ = true;
		if (end_position) {
			switch (anim->type_) {
			case EAnimationType::MOVE:
				anim->a_->SetActorLocation(anim->to_);
				break;
			case EAnimationType::DESTROY:
				anim->a_->SetSomethingHidden(true);
				anim->a_->SetActorScale3D(FVector(1.f));
				break;
			case EAnimationType::DISAPPEAR:
				anim->a_->SetSomethingHidden(true);
				for (UStaticMeshComponent* mesh : anim->a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			case EAnimationType::SHOOT:
				break;
			case EAnimationType::CREATE:
				anim->a_->SetActorLocation(anim->to_);
				anim->a_->SetSomethingHidden(false);
				for (UStaticMeshComponent* mesh : anim->a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			case EAnimationType::TELEPORT:
				anim->a_->SetActorLocation(anim->to_);
				for (UStaticMeshComponent* mesh : anim->a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			case EAnimationType::HEAL:
				break;
			}
		}
	}
}

bool AAnimator::AnimationInProgress() const
{
	return !animations_.IsEmpty();
}

// Called when the game starts or when spawned
void AAnimator::BeginPlay()
{
	Super::BeginPlay();

	game_mode_ = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));
}

// Called every frame
void AAnimator::Tick(const float delta_time)
{
	Super::Tick(delta_time);

	time_ += delta_time;

	TArray<uint32> done_animations;

	for (auto & [idx, anim] : animations_) {
		if (anim.done_) {
			done_animations.Add(idx);
			continue;
		}
		if (time_ < anim.start_time_)
			continue;
		const float t = (time_ - anim.start_time_) / (anim.end_time_ - anim.start_time_);
		switch (anim.type_) {
		case EAnimationType::MOVE:
			if (t >= 1.f) {
				anim.done_ = true;
				anim.a_->SetActorLocation(anim.to_);
				break;
			}
			anim.a_->SetActorLocation(anim.from_ + (anim.to_ - anim.from_) * ((t * (t * 6.f - 15.f) + 10.f) * t * t * t));
			break;
		case EAnimationType::TELEPORT:
			if (t >= 1.f) {
				anim.done_ = true;
				anim.a_->SetActorLocation(anim.to_);
				for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			}
			if (!anim.played_effect_) {
				for (const UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					FVector min, max;
					mesh->GetLocalBounds(min, max);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, teleport_disappear_niagara_, mesh->GetComponentLocation() + FVector(0.f, 0.f, (max.Z - min.Z) * .5f), FRotator::ZeroRotator, max - min);
				}
				anim.played_effect_ = true;
			}
			if (t >= .4f && !anim.played_effect_2_) {
				for (const UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					FVector min, max;
					mesh->GetLocalBounds(min, max);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, teleport_appear_niagara_, anim.to_ - anim.from_ + mesh->GetComponentLocation() + FVector(0.f, 0.f, (max.Z - min.Z) * .5f), FRotator::ZeroRotator, max - min);
				}
				anim.played_effect_2_ = true;
			}
			{
				const float size = 2.f * FMath::Abs(t - .5f);
				FVector min, max;
				anim.a_->GetMeshes()[0]->GetLocalBounds(min, max);
				const FVector offset = FVector(0.f, 0.f, (max.Z - min.Z) * .5f * (1.f - size));
				if (t >= .5f) {
					anim.a_->SetActorLocation(anim.to_ + offset);
				}
				else {
					anim.a_->SetActorLocation(anim.from_ + offset);
				}
				for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(size));
				}
			}
			break;
		case EAnimationType::DESTROY:
			if (t >= 1.f) {
				anim.done_ = true;
				anim.a_->SetSomethingHidden(true);
				anim.a_->SetActorScale3D(FVector(1.f));
				break;
			}
			if (!anim.played_effect_) {
				for (const UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					FVector min, max;
					mesh->GetLocalBounds(min, max);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, explosion_niagara_, mesh->GetComponentLocation() + FVector(0.f, 0.f, (max.Z - min.Z) * .5f), FRotator::ZeroRotator, max - min);
				}
				anim.played_effect_ = true;
			}
			anim.a_->SetActorScale3D(FVector(1.f, 1.f, 1.f - t));
			break;
		case EAnimationType::DISAPPEAR:
			if (t >= 1.f) {
				anim.done_ = true;
				anim.a_->SetSomethingHidden(true);
				for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			}
			for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
				mesh->SetWorldScale3D(FVector(1.f - t));
			}
			break;
		case EAnimationType::SHOOT:
			if (t >= 1.f) {
				anim.done_ = true;
				break;
			}
			if (!anim.played_effect_) {
				UStaticMeshComponent* main_mesh_a = anim.a_->GetMeshes()[0];
				FVector min_main_a, max_main_a;
				main_mesh_a->GetLocalBounds(min_main_a, max_main_a);
				FVector main_a_location = main_mesh_a->GetComponentLocation() + FVector(0.f, 0.f, (max_main_a.Z - min_main_a.Z) * .5f);
				FVector scale = { INFINITY, INFINITY, INFINITY };
				for (UStaticMeshComponent* mesh_b : anim.b_->GetMeshes()) {
					FVector min_b, max_b;
					mesh_b->GetLocalBounds(min_b, max_b);
					const FVector displacement = mesh_b->GetComponentLocation() + FVector(0.f, 0.f, (max_b.Z - min_b.Z) * .5f) - main_a_location;
					if (displacement.Length() < scale.Length())
						scale = displacement;
				}
				for (UStaticMeshComponent* mesh_a : anim.a_->GetMeshes()) {
					FVector min_a, max_a;
					mesh_a->GetLocalBounds(min_a, max_a);
					const FVector from = mesh_a->GetComponentLocation() + FVector(0.f, 0.f, (max_a.Z - min_a.Z) * .5f);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, shoot_laser_niagara_, from, FRotator::ZeroRotator, scale)->SetFloatParameter(TEXT("Damage"), anim.value_);
				}
				anim.played_effect_ = true;
			}
			break;
		case EAnimationType::HEAL:
			if (t >= 1.f) {
				anim.done_ = true;
				break;
			}
			if (!anim.played_effect_) {
				UStaticMeshComponent* main_mesh_a = anim.a_->GetMeshes()[0];
				FVector min_main_a, max_main_a;
				main_mesh_a->GetLocalBounds(min_main_a, max_main_a);
				FVector main_a_location = main_mesh_a->GetComponentLocation() + FVector(0.f, 0.f, (max_main_a.Z - min_main_a.Z) * .5f);
				FVector scale = { INFINITY, INFINITY, INFINITY };
				for (UStaticMeshComponent* mesh_b : anim.b_->GetMeshes()) {
					FVector min_b, max_b;
					mesh_b->GetLocalBounds(min_b, max_b);
					const FVector displacement = mesh_b->GetComponentLocation() + FVector(0.f, 0.f, (max_b.Z - min_b.Z) * .5f) - main_a_location;
					if (displacement.Length() < scale.Length())
						scale = displacement;
				}
				for (UStaticMeshComponent* mesh_a : anim.a_->GetMeshes()) {
					FVector min_a, max_a;
					mesh_a->GetLocalBounds(min_a, max_a);
					const FVector from = mesh_a->GetComponentLocation() + FVector(0.f, 0.f, (max_a.Z - min_a.Z) * .5f);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, heal_laser_niagara_, from, FRotator::ZeroRotator, scale)->SetFloatParameter(TEXT("Damage"), anim.value_);
				}
				anim.played_effect_ = true;
			}
			break;
		case EAnimationType::CREATE:
			if (t >= 1.f) {
				anim.done_ = true;
				anim.a_->SetActorLocation(anim.to_);
				anim.a_->SetSomethingHidden(false);
				for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					mesh->SetWorldScale3D(FVector(1.f));
				}
				break;
			}
			if (!anim.played_effect_) {
				for (const UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
					FVector min, max;
					mesh->GetLocalBounds(min, max);
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, creation_niagara_, mesh->GetComponentLocation() + FVector(0.f, 0.f, (max.Z - min.Z) * .5f), FRotator::ZeroRotator, max - min);
				}
				anim.played_effect_ = true;
			}

			for (UStaticMeshComponent* mesh : anim.a_->GetMeshes()) {
				mesh->SetWorldScale3D(FVector(t));
			}
			anim.a_->SetActorLocation(anim.to_);
			anim.a_->SetSomethingHidden(false);
			break;
		}
	}

	for (const uint32 i: done_animations) {
		animations_.Remove(i);
	}

}

