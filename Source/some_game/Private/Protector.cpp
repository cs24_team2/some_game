// cs24_team2 (c) BY


#include "Protector.h"

#include "AerialUnit.h"
#include "GameplayGameMode.h"
#include "GameField.h"

// Sets default values
AProtector::AProtector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/protector.protector'")));
}


void AProtector::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AProtector::CanAttack(ASomething* target) const {
	if (Cast<AAerialUnit>(target) != nullptr) {
		return false;
	}
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 AProtector::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 AProtector::Damage(int32 damage) {
	if (damage > 5) {
		return DamagePrimitive(damage - 5);
	}
	return DamagePrimitive(0);
}

int32 AProtector::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AProtector::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AProtector::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AProtector::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AProtector::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AProtector::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AProtector::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AProtector::GET_NAME() const
{
	return TEXT("Protector");
}

// Called when the game starts or when spawned
void AProtector::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProtector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

