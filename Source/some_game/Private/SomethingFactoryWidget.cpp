// cs24_team2 (c) BY


#include "SomethingFactoryWidget.h"

#include "Components/Button.h"
#include <Kismet/GameplayStatics.h>

#include "GameplayGameMode.h"
#include "Cell.h"
#include "Something.h"
#include "Building.h"
#include "GroundUnit.h"
#include "AerialUnit.h"

ASomething* USomethingFactoryWidget::Create(const uint8 team)
{
	ASomething* created_something = Cast<ASomething>(GetWorld()->SpawnActor(created_class_));
	GetGameMode()->AddSomething(created_something, created_class_, team);
	return created_something;
}

AGameplayGameMode* USomethingFactoryWidget::GetGameMode()
{
	if (!game_mode_) {
		game_mode_ = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));
	}

	return game_mode_;
}

bool USomethingFactoryWidget::CanBeCreatedOnCell(ACell* cell) const
{
	const UClass* class_ptr = created_class_.Get();
	if (!class_ptr) {
		return false;
	}
	if (Cast<ABuilding>(class_ptr->GetDefaultObject()) && cell->occupying_building_.Last()) {
		return false;
	}
	if (Cast<AGroundUnit>(class_ptr->GetDefaultObject()) && cell->occupying_ground_unit_.Last()) {
		return false;
	}
	if (Cast<AAerialUnit>(class_ptr->GetDefaultObject()) && cell->occupying_aerial_unit_.Last()) {
		return false;
	}
	switch (cell->type_) {
	case CellType::WATER:
		return water_allowed;
	case CellType::GLACIER:
		return glacier_allowed;
	case CellType::RAISED_GLACIER:
		return raised_glacier_allowed;
	case CellType::SNOWY_PLAINS:
		return snowy_plains_allowed;
	case CellType::SNOWY_MOUNTAINS:
		return snowy_mountains_allowed;
	case CellType::PLAINS:
		return plains_allowed;
	case CellType::MOUNTAINS:
		return mountains_allowed;
	case CellType::WASTELAND:
		return wasteland_allowed;
	case CellType::DESERT:
		return desert_allowed;
	default:
		return false;
	}
}

bool USomethingFactoryWidget::IsEnoughResources(const FResources& resources) const
{
	return resources.h_ >= required_H && resources.c_ >= required_C && resources.ae_ >= required_Ae;
}

FResources USomethingFactoryWidget::GetResources() const
{
	return { required_H, required_C, required_Ae };
}

bool USomethingFactoryWidget::CanBeCreated()
{
	const ASomething* something = GetGameMode()->GetSelectedSomething();
	return something && IsEnoughResources(game_mode_->GetTeamResources(game_mode_->GetCurrentTeam())) && CanBeCreatedOnCell(something->GetCell(game_mode_->GetCurrentTurn()));
}
