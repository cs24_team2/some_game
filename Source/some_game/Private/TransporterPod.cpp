// cs24_team2 (c) BY


#include "TransporterPod.h"

#include "Animator.h"
#include "Cell.h"
#include "GameplayGameMode.h"
#include "Transporter.h"

// Sets default values
ATransporterPod::ATransporterPod()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/transporter_pod.transporter_pod'")));
}

bool ATransporterPod::CanTransform() const
{
	ACell* cell = GetCell(current_turn_);
	return cell && !cell->occupying_building_[current_turn_] && cell->type_ != CellType::WATER && cell->type_ != CellType::RAISED_GLACIER && cell->type_ != CellType::SNOWY_MOUNTAINS && cell->type_ != CellType::MOUNTAINS;
}

void ATransporterPod::Transform()
{
	if (CanTransform()) {
		ATransporter* created_transporter = Cast<ATransporter>(GetWorld()->SpawnActor(ATransporter::StaticClass()));
		game_mode_->AddSomething(created_transporter, ATransporter::StaticClass(), team_);
		underlying_cell_.Last()->occupying_building_.Last() = created_transporter;
		created_transporter->Init(underlying_cell_.Last(), current_turn_, team_);
		game_mode_->SetSelectedSomething(created_transporter);
		created_transporter->SET_STATS(TEXT("Health: 40, Sight: 1"));
		created_transporter->SET_DESCRIPTION(TEXT("Ground Units near an allied Transporter can teleport to other allied Transporters. Can be placed only on flat cells."));
		Destroy(false);
		game_mode_->GetAnimator()->AnimateDisappear(this);
	}
}

bool ATransporterPod::CanAttack(ASomething* target) const
{
	return false;
}

int32 ATransporterPod::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 ATransporterPod::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 ATransporterPod::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 ATransporterPod::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

int32 ATransporterPod::GET_VISIBILITY_DISTANCE() const
{
	return VISIBLE_DISTANCE;
}

FString ATransporterPod::GET_NAME() const
{
	return TEXT("Transporter Pod");
}

// Called when the game starts or when spawned
void ATransporterPod::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATransporterPod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

