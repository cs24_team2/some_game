// cs24_team2 (c) BY


#include "Resources.h"

FResources& FResources::operator+=(const FResources& other)
{
	h_ += other.h_;
	c_ += other.c_;
	ae_ += other.ae_;
	return *this;
}

FResources& FResources::operator-=(const FResources& other)
{
	h_ -= other.h_;
	c_ -= other.c_;
	ae_ -= other.ae_;
	return *this;
}

bool FResources::operator<=(const FResources& other) const
{
	return h_ <= other.h_ && c_ <= other.c_ && ae_ <= other.ae_;
}

bool FResources::operator>=(const FResources& other) const
{
	return h_ >= other.h_ && c_ >= other.c_ && ae_ >= other.ae_;
}
