// cs24_team2 (c) BY


#include "FighterJet.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "GroundUnit.h"

// Sets default values
AFighterJet::AFighterJet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/fighter_jet.fighter_jet'")));
}

void AFighterJet::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AFighterJet::CanAttack(ASomething* target) const {
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 AFighterJet::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 AFighterJet::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AFighterJet::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AFighterJet::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AFighterJet::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AFighterJet::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AFighterJet::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AFighterJet::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AFighterJet::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AFighterJet::GET_NAME() const
{
	return TEXT("Fighter Jet");
}

// Called when the game starts or when spawned
void AFighterJet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFighterJet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

