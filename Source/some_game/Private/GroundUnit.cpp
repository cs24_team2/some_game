// cs24_team2 (c) BY


#include "GroundUnit.h"

#include "Animator.h"
#include "Cell.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "Replayer.h"
#include "Transporter.h"

// Sets default values
AGroundUnit::AGroundUnit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

TArray<ACell*> AGroundUnit::GetMoveToCells() const
{
	if (!GetMobility()) {
		return {};
	}

	ACell* cell = GetCell(current_turn_);
	if (!cell) {
		return {};
	}

	TArray<ACell*> neighbours = game_mode_->GetGameField()->GetNeighbours(cell);
	TArray<ACell*> answer;
	for (ACell* neighbour : neighbours) {
		if (CanMoveTo(neighbour)) {
			answer.Add(neighbour);
		}
	}

	if (const ATransporter* ptr = Cast<ATransporter>(cell->occupying_building_[current_turn_]); ptr && ptr->GetTeam() == team_) {
		for (const ASomething* something : game_mode_->GetSomethingsOfClassAndTeam(ATransporter::StaticClass(), team_)) {
			if (ACell* transporter_cell = something->GetCell(current_turn_); transporter_cell && transporter_cell != cell && CanMoveTo(transporter_cell)) {
				answer.Add(transporter_cell);
			}
		}
	}

	return answer;
}

bool AGroundUnit::CanMoveTo(ACell* cell) const
{
	if (cell == GetCell(current_turn_))
		return false;
	if (const ATransporter* ptr = Cast<ATransporter>(cell->occupying_building_[current_turn_]), *ptr2 = Cast<ATransporter>(GetCell(current_turn_)->occupying_building_[current_turn_]); ptr && ptr2 && ptr->GetTeam() == team_ && ptr2->GetTeam() == team_) {
		return true;
	}

	if (cell->type_ == CellType::WATER) {
		return false;
	}
	if (cell->occupying_ground_unit_[current_turn_]) {
		return false;
	}

	return game_mode_->GetGameField()->AreNeighbours(GetCell(current_turn_), cell);
}

void AGroundUnit::Destroy(const bool animate)
{
	GetCell(current_turn_)->occupying_ground_unit_[current_turn_] = nullptr;
	ModifyVisibleCellViewerCounts(current_turn_, -1);
	destruction_turn_ = current_turn_;
	// game_mode_->UpdateSomethingVisibility(this, current_turn_);

	if (animate) {
		// for replay
		const int32 team_count = game_mode_->GetTeamCount();
		AReplayer* replayer = game_mode_->GetReplayer();
		for (int32 i = 0; i < team_count; ++i)
			replayer->AddSomethingChange(this, true, i);

		// animation
		game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
		last_animation_id_ = game_mode_->GetAnimator()->AnimateDestroy(this);
	}
}

// Called when the game starts or when spawned
void AGroundUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGroundUnit::MoveToPrimitive(ACell* cell)
{
	--mobility_;
	const int32 prev_x = GetCell(current_turn_)->x_;
	const ATransporter* transporter_a = Cast<ATransporter>(GetCell(current_turn_)->occupying_building_[current_turn_]), *transporter_b = Cast<ATransporter>(cell->occupying_building_[current_turn_]);
	const bool transporter = transporter_a && transporter_b && transporter_a->GetTeam() == team_ && transporter_b->GetTeam() == team_;
	GetCell(current_turn_)->occupying_ground_unit_[current_turn_] = nullptr;
	cell->occupying_ground_unit_[current_turn_] = this;
	SetCell(cell, current_turn_);

	// animation
	game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
	const FVector from = GetActorLocation() + FVector(100.f * game_mode_->GetGameFieldWidth() * (FMath::Abs(cell->x_ - prev_x) > 3 && !transporter) * (1 - 2 * (cell->x_ < prev_x)), 0.f, 0.f);
	const FVector to = FVector(50. * StaticCast<double>(cell->x_),
		50. * StaticCast<double>(cell->y_) * UE_SQRT_3 + 25. * !!cell->occupying_building_[current_turn_], 
		13.5 + 21.5 * (cell->type_ == CellType::SNOWY_MOUNTAINS || cell->type_ == CellType::MOUNTAINS) + 19.5 * (cell->type_ == CellType::RAISED_GLACIER) + 2. * (cell->type_ == CellType::GLACIER) + 2. * (cell->type_ == CellType::GLACIER && cell->occupying_building_[current_turn_]));

	if (transporter) {
		last_animation_id_ = game_mode_->GetAnimator()->AnimateTeleport(this, from, to);
	}
	else {
		last_animation_id_ = game_mode_->GetAnimator()->AnimateMove(this, from, to);
	}
}

// Called every frame
void AGroundUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

