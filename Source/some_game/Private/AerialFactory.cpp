// cs24_team2 (c) BY


#include "AerialFactory.h"

// Sets default values
AAerialFactory::AAerialFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/BuildingMeshes/aerial_factory.aerial_factory'")));
}

int32 AAerialFactory::Damage(int32 damage)
{
	return DamagePrimitive(damage);
}

int32 AAerialFactory::Heal(int32 heal_amount)
{
	return HealPrimitive(heal_amount);
}

int32 AAerialFactory::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

FString AAerialFactory::GET_NAME() const
{
	return TEXT("Aerial Factory");
}

// Called when the game starts or when spawned
void AAerialFactory::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AAerialFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

