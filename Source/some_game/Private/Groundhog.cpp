// cs24_team2 (c) BY


#include "Groundhog.h"

#include "AerialUnit.h"
#include "Animator.h"
#include "Cell.h"
#include "GameplayGameMode.h"

// Sets default values
AGroundhog::AGroundhog()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/groundhog.groundhog'")));
}

bool AGroundhog::CanSetCheckpoint() const
{
	return warp_;
}

void AGroundhog::SetCheckpoint()
{
	warp_ = false;
	checkpoint_cell_ = GetCell(current_turn_);
	checkpoint_health_ = GetHealth(current_turn_);
	checkpoint_mobility_ = mobility_;
	checkpoint_attack_count_ = attack_count_;
}

bool AGroundhog::CanWarp() const
{
	return warp_ && checkpoint_cell_ && (!checkpoint_cell_->occupying_ground_unit_[current_turn_] || checkpoint_cell_->occupying_ground_unit_[current_turn_] == this);
}

void AGroundhog::Warp()
{
	if (CanWarp()) {
		warp_ = false;
		// teleport
		GetCell(current_turn_)->occupying_ground_unit_[current_turn_] = nullptr;
		checkpoint_cell_->occupying_ground_unit_[current_turn_] = this;
		SetCell(checkpoint_cell_, current_turn_);
		game_mode_->GetAnimator()->StopAnimation(last_animation_id_, true);
		const FVector from = GetActorLocation();
		const FVector to = FVector(50. * StaticCast<double>(checkpoint_cell_->x_),
			50. * StaticCast<double>(checkpoint_cell_->y_) * UE_SQRT_3 + 25. * !!checkpoint_cell_->occupying_building_[current_turn_],
			13.5 + 21.5 * (checkpoint_cell_->type_ == CellType::SNOWY_MOUNTAINS || checkpoint_cell_->type_ == CellType::MOUNTAINS) + 19.5 * (checkpoint_cell_->type_ == CellType::RAISED_GLACIER) + 2. * (checkpoint_cell_->type_ == CellType::GLACIER) + 2. * (checkpoint_cell_->type_ == CellType::GLACIER && checkpoint_cell_->occupying_building_[current_turn_]));
		last_animation_id_ = game_mode_->GetAnimator()->AnimateTeleport(this, from, to);

		health_[current_turn_ - creation_turn_] = checkpoint_health_;
		mobility_ = checkpoint_mobility_;
		attack_count_ = checkpoint_attack_count_;
	}
}

void AGroundhog::ResetWarp()
{
	warp_ = true;
}

bool AGroundhog::CanAttack(ASomething* target) const
{
	if (Cast<AAerialUnit>(target) != nullptr) {
		return false;
	}

	return CanAttackPrimitive(target);
}

int32 AGroundhog::GET_MAX_MOBILITY() const
{
	return MAX_MOBILITY;
}

int32 AGroundhog::GET_MAX_ATTACK_COUNT() const
{
	return MAX_ATTACK_COUNT;
}

int32 AGroundhog::GET_ATTACK_DAMAGE() const
{
	return ATTACK_DAMAGE;
}

int32 AGroundhog::GET_MAX_HEALTH() const
{
	return MAX_HEALTH;
}

int32 AGroundhog::GET_VISIBILITY_DISTANCE() const
{
	return VISIBLE_DISTANCE;
}

int32 AGroundhog::GET_ATTACK_DISTANCE() const
{
	return ATTACK_DISTANCE;
}

FString AGroundhog::GET_NAME() const
{
	return TEXT("Groundhog");
}

// Called when the game starts or when spawned
void AGroundhog::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGroundhog::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

