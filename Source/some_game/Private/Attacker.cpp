// cs24_team2 (c) BY


#include "Attacker.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "AerialUnit.h"

// Sets default values
AAttacker::AAttacker()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/attacker.attacker'")));
}


void AAttacker::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool AAttacker::CanAttack(ASomething* target) const {
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 AAttacker::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 AAttacker::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 AAttacker::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool AAttacker::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> AAttacker::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 AAttacker::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 AAttacker::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 AAttacker::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 AAttacker::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString AAttacker::GET_NAME() const
{
	return TEXT("Attacker");
}

// Called when the game starts or when spawned
void AAttacker::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAttacker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

