// cs24_team2 (c) BY


#include "Commander.h"
#include "GameplayGameMode.h"
#include "GameField.h"
#include "Cell.h"

// Sets default values
ACommander::ACommander()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/commandor.commandor'")));
}

void ACommander::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool ACommander::CanAttack(ASomething* target) const {
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 ACommander::Attack(ASomething* target) {
	return AttackPrimitive(target);
}

int32 ACommander::Damage(int32 damage) {
	int32 result = DamagePrimitive(damage);
	if (result == 0) {
		game_mode_->GameOverForTeam(team_);
	}
	return result;
}

int32 ACommander::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool ACommander::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> ACommander::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 ACommander::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 ACommander::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 ACommander::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 ACommander::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString ACommander::GET_NAME() const
{
	return TEXT("Commander");
}



// Called when the game starts or when spawned
void ACommander::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACommander::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

