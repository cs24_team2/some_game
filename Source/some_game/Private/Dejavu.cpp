// cs24_team2 (c) BY


#include "Dejavu.h"

#include "GameplayGameMode.h"
#include "GameField.h"
#include "Unit.h"
#include "Cell.h"
#include "AerialUnit.h"
#include "Animator.h"
#include "Replayer.h"

// Sets default values
ADejavu::ADejavu()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetMesh(ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Script/Engine.StaticMesh'/Game/SomethingMeshes/UnitMeshes/dejavu.dejavu'")));
}


void ADejavu::MoveTo(ACell* cell) {
	MoveToPrimitive(cell);
}

bool ADejavu::CanAttack(ASomething* target) const {
	if (Cast<AUnit>(target) == nullptr) {
		return false;
	}
	AGameField* game_field = game_mode_->GetGameField();
	if (game_field->GetDistance(GetCell(current_turn_), target->GetCell(current_turn_)) > ATTACK_DISTANCE) {
		return false;
	}
	return team_ != target->GetTeam();
}

int32 ADejavu::Attack(ASomething* target) {
	int32 result = AttackPrimitive(target);
	ACell* current_cell = target->GetCell(current_turn_);
	ACell* last_cell = target->GetCell(current_turn_ - game_mode_->GetTeamCount());
	if (last_cell == current_cell) {
		return target->GetHealth(current_turn_);
	}
	if (last_cell) {
		if (Cast<AGroundUnit>(target)) {
			AGroundUnit* ptr = last_cell->occupying_ground_unit_[current_turn_];

			if (ptr == nullptr) {
				last_cell->occupying_ground_unit_[current_turn_] = Cast<AGroundUnit>(target);
				target->SetCell(last_cell, current_turn_);
				current_cell->occupying_ground_unit_[current_turn_] = nullptr;
				// animation
				game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
				const FVector from = target->GetActorLocation();
				const FVector to = FVector(50. * StaticCast<double>(last_cell->x_),
					50. * StaticCast<double>(last_cell->y_) * UE_SQRT_3 + 25. * !!last_cell->occupying_building_[current_turn_],
					13.5 + 21.5 * (last_cell->type_ == CellType::SNOWY_MOUNTAINS || last_cell->type_ == CellType::MOUNTAINS) + 19.5 * (last_cell->type_ == CellType::RAISED_GLACIER) + 2. * (last_cell->type_ == CellType::GLACIER) + 2. * (last_cell->type_ == CellType::GLACIER && last_cell->occupying_building_[current_turn_]));
				target->GetLastAnimationId() = game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
				return 0;
			}

			if (ptr->GetHealth(current_turn_) >= target->GetHealth(current_turn_)) {
				ptr->Damage(target->GetHealth(current_turn_));
				// teleport animation
				game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
				const FVector from = target->GetActorLocation();
				const FVector to = ptr->GetActorLocation();
				game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
				target->Destroy(false);
				// for destroy animation and replay
				const int32 team_count = game_mode_->GetTeamCount();
				AReplayer* replayer = game_mode_->GetReplayer();
				AAnimator* animator = game_mode_->GetAnimator();
				for (int32 i = 0; i < team_count; ++i)
					replayer->AddSomethingChange(target, true, i);

				if (last_cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
					animator->StopAnimation(target->GetLastAnimationId(), true);
					target->GetLastAnimationId() = animator->AnimateDestroy(target, .5f);
				}
				else {
					replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), target, nullptr, 1.f);
				}
				return target->GetHealth(current_turn_);
			}

			target->Damage(ptr->GetHealth(current_turn_));

			last_cell->occupying_ground_unit_[current_turn_] = Cast<AGroundUnit>(target);
			target->SetCell(last_cell, current_turn_);
			current_cell->occupying_ground_unit_[current_turn_] = nullptr;
			// teleport animation
			game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
			const FVector from = target->GetActorLocation();
			const FVector to = FVector(50. * StaticCast<double>(last_cell->x_),
				50. * StaticCast<double>(last_cell->y_) * UE_SQRT_3 + 25. * !!last_cell->occupying_building_[current_turn_],
				13.5 + 21.5 * (last_cell->type_ == CellType::SNOWY_MOUNTAINS || last_cell->type_ == CellType::MOUNTAINS) + 19.5 * (last_cell->type_ == CellType::RAISED_GLACIER) + 2. * (last_cell->type_ == CellType::GLACIER) + 2. * (last_cell->type_ == CellType::GLACIER && last_cell->occupying_building_[current_turn_]));
			target->GetLastAnimationId() = game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
			ptr->Destroy(false);
			// for destroy animation and replay
			const int32 team_count = game_mode_->GetTeamCount();
			AReplayer* replayer = game_mode_->GetReplayer();
			AAnimator* animator = game_mode_->GetAnimator();
			for (int32 i = 0; i < team_count; ++i)
				replayer->AddSomethingChange(ptr, true, i);

			if (last_cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
				animator->StopAnimation(ptr->GetLastAnimationId(), true);
				ptr->GetLastAnimationId() = animator->AnimateDestroy(ptr, .5f);
			}
			else {
				replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), ptr, nullptr, 1.f);
			}
			return target->GetHealth(current_turn_);
		}
		else {
			AAerialUnit* ptr = last_cell->occupying_aerial_unit_[current_turn_];

			if (ptr == nullptr) {
				last_cell->occupying_aerial_unit_[current_turn_] = Cast<AAerialUnit>(target);
				target->SetCell(last_cell, current_turn_);
				current_cell->occupying_aerial_unit_[current_turn_] = nullptr;
				// animation
				game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
				const FVector from = target->GetActorLocation();
				const FVector to = FVector(50. * StaticCast<double>(last_cell->x_),
					50. * StaticCast<double>(last_cell->y_) * UE_SQRT_3 + 10.,
					60.);
				target->GetLastAnimationId() = game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
				return target->GetHealth(current_turn_);
			}

			if (ptr->GetHealth(current_turn_) >= target->GetHealth(current_turn_)) {
				ptr->Damage(target->GetHealth(current_turn_));
				// teleport animation
				game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
				const FVector from = target->GetActorLocation();
				const FVector to = ptr->GetActorLocation();
				game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
				target->Destroy(false);
				// for destroy animation and replay
				const int32 team_count = game_mode_->GetTeamCount();
				AReplayer* replayer = game_mode_->GetReplayer();
				AAnimator* animator = game_mode_->GetAnimator();
				for (int32 i = 0; i < team_count; ++i)
					replayer->AddSomethingChange(target, true, i);

				if (last_cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
					animator->StopAnimation(target->GetLastAnimationId(), true);
					target->GetLastAnimationId() = animator->AnimateDestroy(target, .5f);
				}
				else {
					replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), target, nullptr, 1.f);
				}
				return 0;
			}

			target->Damage(ptr->GetHealth(current_turn_));

			last_cell->occupying_aerial_unit_[current_turn_] = Cast<AAerialUnit>(target);
			target->SetCell(last_cell, current_turn_);
			current_cell->occupying_aerial_unit_[current_turn_] = nullptr;
			// teleport animation
			game_mode_->GetAnimator()->StopAnimation(target->GetLastAnimationId(), true);
			const FVector from = target->GetActorLocation();
			const FVector to = FVector(50. * StaticCast<double>(last_cell->x_),
				50. * StaticCast<double>(last_cell->y_) * UE_SQRT_3 + 10.,
				60.);
			target->GetLastAnimationId() = game_mode_->GetAnimator()->AnimateTeleport(target, from, to);
			ptr->Destroy(false);
			// for destroy animation and replay
			const int32 team_count = game_mode_->GetTeamCount();
			AReplayer* replayer = game_mode_->GetReplayer();
			AAnimator* animator = game_mode_->GetAnimator();
			for (int32 i = 0; i < team_count; ++i)
				replayer->AddSomethingChange(ptr, true, i);

			if (last_cell->ViewerCounts(game_mode_->GetCurrentTeam(), current_turn_)) {
				animator->StopAnimation(ptr->GetLastAnimationId(), true);
				ptr->GetLastAnimationId() = animator->AnimateDestroy(ptr, .5f);
			}
			else {
				replayer->AddAnimationToReplay(EAnimationType::DESTROY, FVector(0.f), FVector(0.f), ptr, nullptr, 1.f);
			}
			return target->GetHealth(current_turn_);
		}
	}
	else {
		target->Destroy();
		return 0;
	}
}

int32 ADejavu::Damage(int32 damage) {
	return DamagePrimitive(damage);
}

int32 ADejavu::Heal(int32 heal_amount) {
	return HealPrimitive(heal_amount);
}

bool ADejavu::IsVisible(ACell* cell, uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	return game_field->GetDistance(GetCell(turn), cell) <= VISIBLE_DISTANCE;
}

TArray<ACell*> ADejavu::GetVisibleCells(uint32 turn) const {
	AGameField* game_field = game_mode_->GetGameField();
	TArray<ACell*> result = game_field->GetCellsAtDistance(GetCell(turn), VISIBLE_DISTANCE);
	result.Add(GetCell(turn));
	return result;
}

int32 ADejavu::GET_MAX_MOBILITY() const {
	return MAX_MOBILITY;
}

int32 ADejavu::GET_MAX_ATTACK_COUNT() const {
	return MAX_ATTACK_COUNT;
}

int32 ADejavu::GET_ATTACK_DAMAGE() const {
	return ATTACK_DAMAGE;
}

int32 ADejavu::GET_MAX_HEALTH() const {
	return MAX_HEALTH;
}

FString ADejavu::GET_NAME() const
{
	return TEXT("Dejavu");
}

// Called when the game starts or when spawned
void ADejavu::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADejavu::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

